## Description
This project contains poetry written or translated by Jesse Crossen, along with
code and configuration to compile it into various publication formats.

## Building
Creating publishable versions of the content requires
[GNU Make](https://www.gnu.org/software/make/) and
[LuaTeX](http://luatex.org/). The build process has only been tested on
Debian-based Linux distributions (e.g. Ubuntu). The following should install
the required dependencies:

```
$ sudo apt install make texlive-luatex \
                        texlive-latex-recommended \
                        texlive-latex-extra
```

To build all targets, run `make` from the repository root.

## License
[Attribution-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/)
