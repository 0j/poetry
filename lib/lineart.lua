module(...,package.seeall)

function svgpath(path, style)
    if not string.find(path, ".svg$") then
        path = path .. ".svg"
    end
    style = style or 'color=black,thick'
    -- load the SVG
    local f = assert(io.open(path, "r"))
    svg = f:read("*all")
    f:close()
    -- convert screen-type coords to math-type coords
    tex.print('\\begin{scope}[yscale=-1]')
    -- extract paths
    for d in string.gmatch(svg, ' d="[^"]+"') do
        d = string.gsub(d, '^[^"]*"', '')
        d = string.gsub(d, '"*$', '')
        tex.print('\\draw['..style..'] svg "'..d..'";')
    end
    -- close the scope
    tex.print('\\end{scope}')
end
