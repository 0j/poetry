module(...,package.seeall)

local simplemd = require("simplemd")

function defaultoptions(path)
    opt = {}
    -- path to markdown source
    opt.path = path
    -- leading string to treat as an indent
    opt.indentin = "    "
    -- space to indent for each occurence of indentin
    opt.indentdim = "1.5em"
    -- space to leave between stanzas
    opt.stanzadim = "0.5em"
    -- whether to number lines
    opt.numbered = false
    -- range of line numbers to output
    opt.firstline = 0
    opt.lastline = 1000000
    return opt
end

function parseoptions(path, optstr)
    local opt = defaultoptions(path)
    for key, value in string.gmatch(optstr, "%s*([^,=]+)=([^,]+)%s*") do
        assert(opt[key] ~= nil, "unrecognized option key: "..key)
        if type(opt[key]) == "boolean" then
            value = (value == "true")
        elseif type(opt[key]) == "number" then
            value = tonumber(value)
        end
        opt[key] = value
    end
    return opt
end

function decoratetitle(title, opt)
    if opt.firstline == 0 then
        return string.format("\\mdpoemtitle{%s}", title)
    end
    return ""
end

function decorateline(line, lineno, opt)
    local prefix = ""
    local body = line
    local suffix = ""
    -- add line numbers
    if opt.numbered then
        prefix = (prefix..
            "\\makebox[1em][r]"..
            "{\\scriptsize "..lineno.."}\\hspace{0.25em}")
    end
    -- add indentation
    while string.find(body, "^"..opt.indentin) do
        body = string.gsub(body, "^"..opt.indentin, "")
        prefix = prefix.."\\hspace{"..opt.indentdim.."} "
    end
    -- add a break
    suffix = suffix .. " \\mdpoembreak  "
    -- convert ascii typographical symbols to unicode
    body = simplemd.typography(body)
    -- convert styled spans to tex
    body = simplemd.styles(body)
    return prefix..body..suffix
end

function stanzabreak(opt)
    return "\\vspace{"..opt.stanzadim.."}"
end

function linestex(lines, opt, stanzabreaks)
    body = ""
    for lineno, line in pairs(lines) do
        if lineno >= opt.firstline and lineno <= opt.lastline then
            body = body .. decorateline(line, lineno, opt)
            if stanzabreaks[lineno] then
                body = body .. stanzabreak(opt)
            end
        end
    end
    if body then
        return "\\mdpoembody{" .. body .. "}"
    end
    return ""
end

function poem(opt)
    -- path to the file to load
    if type(opt.path) ~= "string" then
        error("no path")
    end
    path = opt.path
    if not string.find(path, ".md$") then
        path = path .. ".md"
    end
    -- open the markdown source
    local f = assert(io.open(path, "r"))
    local poemlines = { }
    local lineno = 0
    local titletex = ""
    local stanzabreaks = { }
    for line in f:lines() do
        -- trim trailing whitespace
        line = string.gsub(line, "%s+$", "")
        -- match a title
        if string.find(line, "^#+ ") then
            titletex = decoratetitle(string.gsub(line, "^#+ ", ""), opt)
        -- match a blank line
        elseif line == "" then
            -- only count stanza breaks after the first non-empty line
            if lineno > 0 then
                stanzabreaks[lineno] = true
            end
        else
            lineno = lineno + 1
            poemlines[lineno] = line
        end
    end
    f:close()
    tex.print(titletex)
    tex.print(linestex(poemlines, opt, stanzabreaks))
end
