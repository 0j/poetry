module(...,package.seeall)

-- convert ascii to typographical symbols
function typography(line)
    -- open double (" or ``)
    line = string.gsub(line, '(%s)"', '%1“')
    line = string.gsub(line, '^"', '“')
    line = string.gsub(line, "``", '“')
    -- close double (" or '')
    line = string.gsub(line, '"', '”')
    line = string.gsub(line, "''", '”')
    -- open single (' or `)
    line = string.gsub(line, "(%s)'", '%1‘')
    line = string.gsub(line, "^'", '‘')
    line = string.gsub(line, "`", '‘')
    -- close single (')
    line = string.gsub(line, "'", "’")
    -- em dash (---)
    line = string.gsub(line, '%-%-%-', '—')
    -- en dash (---)
    line = string.gsub(line, '%-%-', '–')
    -- ellipsis (...)
    line = string.gsub(line, '%.%.%.', '…')
    return line
end

function span(line, marker, command)
    boundchar = '[ ,.:;"\'`“”‘’…()—–-]'
    line = string.gsub(line, '^'..marker..'([^.])', '\\'..command..'{%1')
    line = string.gsub(line, '('..boundchar..')'..marker..'(%S)', '%1\\'..command..'{%2')
    line = string.gsub(line, marker..'$', '}')
    line = string.gsub(line, '(%S)'..marker..'('..boundchar..')', '%1}%2')
    return line
end

-- style spans of text
function styles(line)
    local body = md
    -- bold
    line = span(line, '%*%*', 'textbf')
    -- italics
    line = span(line, '%*', 'textit')
    -- international
    line = span(line, '~', 'textintl')
    return line
end

-- style lists of things
function lists(line)
    -- numbered list
    line = string.gsub(line, '^%d+%.%s*', '\\item ')
    -- bullet list
    line = string.gsub(line, '^%*%s', '\\item ')
    return line
end

-- style links
function links(line)
    line = string.gsub(line, '%[(.-)%]%(https://(.-)%)', '%1 (%2)')
    return line
end

-- apply all transforms
function all(line)
    return links(styles(lists(typography(line))))
end

-- update the context from the given line
function getcontext(line, context)
    local empty = line == ''
    local piped = string.find(line, '^|')
    local numbered = string.find(line, '^%d+%.')
    local starred = string.find(line, '^%*%s')
    if piped then
        return 'poem'
    elseif context == 'poem' and not piped then
        return nil
    elseif numbered then
        return 'ol'
    elseif starred then
        return 'ul'
    elseif empty then
        return nil
    elseif context == nil then
        return 'par'
    end
    return context
end

-- return opening tex for a context
function opencontext(context)
    if context == 'poem' then
        return '\\mdpoembody{'
    elseif context == 'ol' then
        return '\\begin{enumerate}'
    elseif context == 'ul' then
        return '\\begin{itemize}'
    end
    return ''
end

-- return closing tex for a context
function closecontext(context)
    if context == 'poem' then
        return '}'
    elseif context == 'ol' then
        return '\\end{enumerate}'
    elseif context == 'ul' then
        return '\\end{itemize}'
    end
    return ''
end
