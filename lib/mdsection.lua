module(...,package.seeall)

local mdpoem = require("mdpoem")
local simplemd = require("simplemd")

function defaultoptions(path)
    opt = {}
    -- path to markdown source
    opt.path = path
    return opt
end

function parseoptions(path, optstr)
    local opt = defaultoptions(path)
    for key, value in string.gmatch(optstr, "%s*([^,=]+)=([^,]+)%s*") do
        assert(opt[key] ~= nil, "unrecognized option key: "..key)
        if type(opt[key]) == "boolean" then
            value = (value == "true")
        elseif type(opt[key]) == "number" then
            value = tonumber(value)
        end
        opt[key] = value
    end
    return opt
end

-- format blocks starting with a pipe as verse
function verse(text)
    return string.gsub(text, "(\n)(| .-)(\n[^|])", replaceverse)
end
function replaceverse(prefix, body, suffix)
    local lines = {}
    local stanzabreaks = {}
    local lineno = 0
    for line in string.gmatch("\n"..body, "\n|([^\n]*)") do
        line = string.gsub(line, "^ ", "")
        line = string.gsub(line, "%s+$", "")
        if line == "" then
            if lineno > 0 then
                stanzabreaks[lineno] = true
            end
        else
            lineno = lineno + 1
            lines[lineno] = line
        end
    end
    opt = mdpoem.defaultoptions("")
    opt.styledtext = false
    return prefix .. mdpoem.linestex(lines, opt, stanzabreaks) .. suffix
end

function section(opt)
    -- path to the file to load
    if type(opt.path) ~= "string" then
        error("no path")
    end
    path = opt.path
    if not string.find(path, ".md$") then
        path = path .. ".md"
    end
    -- open the markdown source
    local f = assert(io.open(path, "r"))
    local output = ''
    local context = nil
    local poemopt = mdpoem.defaultoptions("")
    for line in f:lines() do
        line = string.gsub(line, '\n+$', '')
        -- update the context
        local newcontext = simplemd.getcontext(line, context)
        if newcontext ~= context then
            output = output .. simplemd.closecontext(context)
            if output ~= '' and not context and newcontext == 'par' then
                output = output .. '\\par '
            end
            context = newcontext
            output = output .. simplemd.opencontext(context)
        end
        -- process the line in the current context
        if context == 'poem' then
            if line == '|' then
                output = output .. mdpoem.stanzabreak(poemopt)
            else
                output = output .. mdpoem.decorateline(string.sub(line, 2), 1, poemopt)
            end
        elseif context then
            output = output .. simplemd.all(line) .. ' '
        end
    end
    f:close()
    output = output .. simplemd.closecontext(context) .. '\n'
    tex.print(output)
end
