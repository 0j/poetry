\documentclass[12pt]{book}

% metadata
\usepackage{titling}
\title{Piercing the Heart}
\author{Yunus Emre}

% custom libraries
\usepackage{simplemd}
\usepackage{lineart}

% calculate print size for a full bleed interior
\newcommand{\finalwidth}{5in}
\newcommand{\finalheight}{8in}
\newcommand{\edgetrim}{0.125in}
\newcommand{\printwidth}{\dimexpr\finalwidth+\edgetrim\relax}
\newcommand{\printheight}{\dimexpr\edgetrim+\finalheight+\edgetrim\relax}
% specify layout
\usepackage[paperwidth=\printwidth,
            paperheight=\printheight,
            headsep=1em]{geometry}
\raggedbottom
\newcommand{\begintitle}{%
    \newgeometry{inner=0.875in, outer=0.625in, top=0.75in, bottom=0.875in} %
    \pagestyle{empty} %
}
\newcommand{\beginfrontmatter}{%
    \newgeometry{inner=0.875in, outer=0.625in, top=0.75in, bottom=0.875in} %
    \pagestyle{headings} %
}
\newcommand{\beginpoems}{%
    \newgeometry{inner=1.125in, outer=0.875in, top=0.75in, bottom=0.875in} %
}
\newcommand{\beginbackmatter}{%
    \newgeometry{inner=0.875in, outer=0.625in, top=0.75in, bottom=0.875in} %
}

% specify fonts
\usepackage{fontspec}
\setmainfont[BoldFont=Whitman-BoldOsF,
             ItalicFont=Whitman-ItalicOsF,
             SmallCapsFont=Whitman-SmallCapsOsF]
             {Whitman-RomanOsF}
\renewcommand\textintl[1]{{\fontspec{Noto Serif}\footnotesize#1}}
% reduce leading
\linespread{0.931}

% use web-style paragraphs with no indent
\usepackage[parfill]{parskip}

% customize titles
\usepackage{titlesec}
\usepackage{ifthen}
\titleformat{\section}[block]
            {\scshape\large}
            {\thesection}{0.5em}{}
\newcommand\about[2]{
    \section*{#1}
    \mdsection{#2}
}
\renewcommand\mdpoemtitle[1]{
    \ifthenelse{\equal{#1}{}}{}{\addcontentsline{toc}{section}{#1}}
    \section*{#1}
}

% customize headings
\usepackage{titleps}
\renewpagestyle{plain}{
    \sethead[][][]{}{}{}
    \setfoot[][][]{}{}{}
}
\renewpagestyle{headings}{
    \sethead[][][]{}{}{}
    \setfoot[\thepage][][]{}{}{\thepage}
}
\newpagestyle{outshift1}{
    \sethead[][][]{}{}{}
    \setfoot[\hspace{-1em}\thepage][][]{}{}{\thepage\hspace{-1em}}
}
\newpagestyle{outshift2}{
    \sethead[][][]{}{}{}
    \setfoot[\hspace{-2em}\thepage][][]{}{}{\thepage\hspace{-2em}}
}
\newpagestyle{inshift1}{
    \sethead[][][]{}{}{}
    \setfoot[\hspace{1em}\thepage][][]{}{}{\thepage\hspace{1em}}
}
\newpagestyle{inshift2}{
    \sethead[][][]{}{}{}
    \setfoot[\hspace{2em}\thepage][][]{}{}{\thepage\hspace{2em}}
}

% customize table of contents
\usepackage{tocloft}
\renewcommand{\cfttoctitlefont}{\scshape\large}
\setlength{\cftbeforetoctitleskip}{0em}
\setlength{\cftaftertoctitleskip}{0.5em}
\AtBeginDocument{\addtocontents{toc}{\protect\thispagestyle{empty}}}

\newcommand*\cleartoverso{%
  \clearpage
  \ifodd\value{page}\hbox{}\newpage\fi
}

% format links on the title page
\newcommand{\linkline}[1]{\hbox{\small\textbf{\texttt{#1}}}}

% add a little looseness to the line-breaking
\setlength{\emergencystretch}{1em}
% reduce widows and orphans
\clubpenalty=300
\widowpenalty=300

% testing and debugging
% \usepackage{debug}
% \renewcommand\pagelineart[2]{.} % uncomment to disable graphics
\newif\iffull
\fulltrue

\begin{document}

    \begintitle

    % title page
    \begin{center}
        \scshape
        \vspace*{2in}
        {\Huge\thetitle} \\
        \vspace{0.25em}
        \hspace{-3.5em}poems by \\
        \vspace{0.5em}
        {\Huge\theauthor} \\
        \vspace{\fill}
        {\small translated by} \\
        {\large Jesse Crossen}
    \end{center}
    \clearpage

    % copyright page
    \begin{flushleft}
        \thetitle \\
        Copyright © 2022 by Jesse Crossen \\
        This edition was compiled \today \\
        \vspace{1em}
        {\normalsize\fontspec{CC Icons} c\hspace{4pt}b\hspace{4pt}a} \\
        This work is licensed under the Creative Commons
        Attribution-ShareAlike 4.0 International License. \\
        To view a copy of this license, visit
        \linkline{http://creativecommons.org/licenses/by-sa/4.0/} \\
        or send a letter to \\
        Creative Commons, \\
        PO Box 1866, \\
        Mountain View, CA 94042, USA. \\
        \vspace{1em}
        ISBN 979-8-83-566727-7 \\
        \vspace{1em}
        Design and illustrations by Jesse Crossen \\
        \vspace{1em}
        \textsc{Hinterlander} \\
        500 Westover Dr \#16346 \\
        Sanford, NC 27330
        \linkline{https://hinterlander.substack.com} \\
    \end{flushleft}
    \clearpage

    % Table of Contents
    \tableofcontents
    \clearpage

    \beginfrontmatter

    \addcontentsline{toc}{section}{\textit{Notes}}
    \about{About the Poet}{sections/about-poet.md}
    \clearpage
    \about{Translation Notes}{sections/translation-notes.md}
    \clearpage
    \about{Cultural Parallels}{sections/cultural-references.md}
    \clearpage
    \about{About the Poetic Forms}{sections/about-ghazal.md}
    \about{About the Source Material}{sections/about-source.md}
    \about{About the Illustrations}{sections/about-illustrations.md}
    \clearpage
    \about{About the Translator}{sections/about-translator.md}

\iffull

    \beginpoems

    % With the Mountains (VR)
    \cleartoverso
    \beginpoems
    \pagelineart{graphics/mountains}{0}
    \vspace*{\fill}
    \mdpoem{poems/wtmwts/poem}
    \clearpage
    \pagelineart{graphics/mountains}{1}
    \clearpage

    % One Word (VR)
    \cleartoverso
    \mdpoem[lastline=21]{poems/oneword/poem}
    \pagelineart{graphics/sprouts}{0}
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=22]{poems/oneword/poem}
    \pagelineart{graphics/sprouts}{1}
    \clearpage
    % Spring (VRV)
    \cleartoverso
    \mdpoem[lastline=28]{poems/spring/poem}
    \pagelineart{graphics/spring}{0}
    \clearpage
    \vspace*{\fill}
    \mdpoem[firstline=29,lastline=52]{poems/spring/poem}
    \vspace{-0.25in}
    \pagelineart{graphics/spring}{1}
    \clearpage
    \mdpoem[firstline=53]{poems/spring/poem}
    \pagelineart{graphics/roses}{0}
    \thispagestyle{outshift2} % page number interferes with flower
    \clearpage
    % It Comes and Goes (R)
    \mdpoem{poems/icag/poem}
    \pagelineart{graphics/roses}{1}
    \thispagestyle{outshift2} % page number interferes with flower
    \clearpage
    % I Need it Back (V)
    \cleartoverso
    \mdpoem{poems/inib/poem}
    \clearpage
    % That Pearl (R)
    \mdpoem{poems/pearl/poem}
    \pagelineart{graphics/pearl}{1}
    \clearpage
    % Let Down Thy Veil (X)
    \mdpoem{poems/ldyv/poem}
    \pagelineart{graphics/candle}{0}
    \clearpage
    % You're the One For Me (X)
    \mdpoem{poems/ytofminy/poem}
    \clearpage
    % Going Home (VR)
    \cleartoverso
    \pagelineart{graphics/goinghome}{0}
    \clearpage
    \mdpoem{poems/goinghome/poem}
    \clearpage
    % Hey Lover Boy (VR)
    \cleartoverso
    \mdpoem[lastline=24]{poems/hlb/poem}
    \pagelineart{graphics/flowersgrave}{0}
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=25]{poems/hlb/poem}
    \pagelineart{graphics/flowersgrave}{1}
    \thispagestyle{outshift2} % page number interferes with skeleton
    \clearpage
    % Lying Down (X)
    \mdpoem{poems/ld/poem}
    \clearpage
    % Crying (R)
    \mdpoem{poems/crying/poem}
    \pagelineart{graphics/bookofdeeds}{1}
    \clearpage
    % I Came (VR)
    \cleartoverso
    \mdpoem[lastline=25]{poems/icame/poem}
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=26]{poems/icame/poem}
    \clearpage
    % Poison (V)
    \cleartoverso
    \mdpoem{poems/poison/poem}
    \pagelineart{graphics/poison}{0}
    \clearpage
    % Let There Be Separation (R)
    \mdpoem{poems/ltbs/poem}
    \pagelineart{graphics/poison}{1}
    \thispagestyle{empty} % page number interferes with drawing
    \clearpage
    % You Can't (V)
    \cleartoverso
    \mdpoem{poems/ycbad/poem}
    \clearpage
    % What's Wrong With Me (R)
    \mdpoem{poems/wwwm/poem}
    \pagelineart{graphics/diving}{1}
    \thispagestyle{inshift2} % page number interferes with dolphin
    \clearpage
    % Come See (VR)
    \cleartoverso
    \mdpoem[lastline=16]{poems/cswlhdtm/poem}
    \pagelineart{graphics/comesee}{0}
    \thispagestyle{inshift2} % page number interferes with robe
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=17]{poems/cswlhdtm/poem}
    \pagelineart{graphics/comesee}{1}
    \thispagestyle{empty} % page number interferes with ground
    \clearpage
    % The Hour of Death (VR)
    \cleartoverso
    \mdpoem[lastline=16]{poems/thod/poem}
    \pagelineart{graphics/tomb}{0}
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=17]{poems/thod/poem}
    \pagelineart{graphics/tomb}{1}
    \thispagestyle{outshift2} % page number interferes with drawing
    \clearpage
    % If That's the Way (X)
    \mdpoem{poems/ittw/poem}
    \pagelineart{graphics/theway}{0}
    \clearpage
    % Mine (X)
    \mdpoem{poems/mine/poem}
    % Inside of Me (VR)
    \cleartoverso
    \mdpoem[lastline=24]{poems/inside/poem}
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=25]{poems/inside/poem}
    \pagelineart{graphics/prison}{1}
    \clearpage
    % Oh Nightingale (X)
    \mdpoem{poems/on/poem}
    \clearpage
    % I Don't Have any Wish to Fly (X)
    \mdpoem{poems/idhawtf/poem}
    % Life Slips Through Our Fingers (VR)
    \cleartoverso
    \mdpoem{poems/lstof/poem}
    \clearpage
    \pagelineart{graphics/lstof}{1}
    \clearpage
    % Plundered (X)
    \mdpoem[lastline=20]{poems/plundered/poem}
    \pagelineart{graphics/hive}{0}
    \clearpage
    \mdpoemtitle{} % balances title on verso page
    \mdpoem[firstline=21]{poems/plundered/poem}
    \pagelineart{graphics/hive}{1}
    \clearpage

\fi

    \beginbackmatter

    % Thanks
    \addcontentsline{toc}{section}{\textit{Thanks}}
    \about{Thanks}{sections/thanks.md}
    \clearpage

    % Tools and Resources
    \begin{flushleft}
        \about{Tools and Resources}{sections/tools.md}
    \end{flushleft}

\end{document}
