# Lying Down

At dawn I reached a field of headstones,
    beds for corpses lying down.
They got too sick to cure and then
    their lifetime ended lying down.

Here a husband, here a soldier,
    here a lord, and here a leper
Spent their day for worse or better,
    darkness found them lying down.

I reached the graveyard in a hearse,
    looked up at death's almighty purse
That pays the dead their just deserts,
    and death was not yet lying down.

My neighbors with their chests caved in
    while maggots wandered on their skin,
And young girls with a child in them,
    sweet roses withered lying down.

They left behind unfinished work,
    their pearly teeth adorn the earth,
Their scattered hairs enlace the dirt,
    the ground embraced them lying down.

While bodies rot and reach their end,
    their souls reach out to find the Friend,
And don't you see you're one of them?
Our time has come for lying down.
