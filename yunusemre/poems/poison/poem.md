# Poison

Friend, if I bring you news of your homeland,
    will you believe it?
Fellow traveler, when you know the way there,
    will you take it?
You can travel back there by magic,
    just put some poison in your wine.
When you can't hold the cup of poison anymore,
    won't you stop drinking?
Once your hand is dead,
    it can't hold onto suffering.
If it was made into a sweetmeat just for you,
    would you taste the poison?
Back home there is no sun and moon,
    no way to count the days and months.
If you could leave your troubles behind,
    would you need to count the days?
If you could leave yourself behind,
    fall into the hands of emptiness,
    drink from the pure ecstasy of love,
    wouldn't you drop all this earthly baggage?
This body of yours is just a pattern
    of fire, wind, earth, and water.
Yunus, think about the essence,
    is it just water and earth you're made of?
