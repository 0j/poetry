# Ağı

Dost ilinin haberin desem
    işite misin?
Yoldaş olup o yola sen bile
    gide misin?
O ilin bağı olur,
    şerbeti ağı olur,
Kadeh tutmaz o ağı,
    nuş edip yuda mısın?
O elin zevadesi,
    cefa tuta gidesi.
Şekeri ayrı sunup,
    sen ağı tada mısın?
O ilde ay gün olmaz,
    ay gün gidilip dolmaz,
Tertipler terk idüben,
    hesap unuda mısın?
Senlik benlik terk edip,
    yokluk eline gidip,
Aşktan içip esriyip,
    varlık terk ede misin?
İşbu tenin tertibi,
    ateş, yel, toprak, sudur,
Yunus sen gör özünü,
    suda, toprakta mısın?
