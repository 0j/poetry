# Let There Be Separation

With nobody else around,
    you can't get offended
Stay on dry land,
    and you'll never find a pearl
You can't walk on the road
    unless somebody cleared it for you
The full moon can only rise
    when the sun sets
A listener won't catch your drift
    if they're a fool
A heart cannot be pierced
    if it's made out of stone
If your walking stick breaks in two,
    it won't go back together
That's how it is!
    No ointment will heal it
A traveler sets out to cross the desert,
    it looks easy standing in the shade
Let there be separation,
    you'll have oneness soon enough
One soul separated from all the others
    can't settle down
Yunus, give your soul
    to the path of Truth
If you don't give your soul,
    you'll never find your beloved
