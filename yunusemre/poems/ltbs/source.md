# Bırak İkiliği

Sen sende iken
    menzil alınmaz
Bahrî olmadan
    gevher bulunmaz
Er açtı yolu
    ayan eyledi
Tolun ay doğdu,
    hergiz tolunmaz
Er mânasından
    almayan cahil
Taştandır bağrı
    hergiz delinmez
Er urdu yare,
    zahmi belirmez
Var! Bu yareme,
    merhem bulunmaz
Mağripten meşrık erin gözüne
    ayan görüne perde olunmaz
Ko ikiliği,
    gel birliğe yet
Bir olan canlar
    ayrı dölenmez
Yunus, ver canını
    Hak yoluna
Can vermeyince
    canan bulunmaz
