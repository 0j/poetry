# Inside of Me

God's in the village square,
    like me a traveller.
I feel it in the air,
    God is inside of me.

Suffering apart from you,
    I hid my heart from you.
Cleansing my inner truth,
    God is inside of me.

What if my house burns down,
    won't leave a scar on me.
Walking on poisoned ground
    won't be the death of me.

That God's apart from me,
    that I cannot believe.
I have searched endlessly,
    God is inside of me.

Life is a prison cell,
    we're doing time in it.
Prayer is our water well,
    faith is the bread we get.

Preachers stop by to preach,
    selling delicious treats.
I have enough to eat,
    God is inside of me.

So close to God though, won't it
    burn you, Yunus?
Won't your heart that can't stop bleeding
    drain you, Yunus?
What will serving forty years
    return you, Yunus?
I hid away my diamonds,
    God is inside of me.

What the Prophet taught us won't
    forsake you, Yunus.
Hold yourself apart and God can't
    take you, Yunus.
Just let Mohammed's light
    illuminate you, Yunus.
I embraced eternity,
    God is inside of me.
