# Hak Bendedir Bende

Hak ile pazarım,
    bile gezerim,
Dayim sezerim,
    Hak bendedir bende.

Ben yadda kaldım,
    kalbe saklandım,
Sıdkım pakladım,
    Hak bendedir bende.

Ateşe girersem
    yanmazım asla,
Hem zehir yer isem
    ölmezim asla.

Hak'tan ayrı mıyım,
    sanmazım asla,
Pek çok yokladım,
    Hak bendedir bende.

Burası nadana
    pek uzacıktır,
Züht ve takva
    insana tuzacıktır.

Mürşide hizmeti
    bir mezeciktir,
Nutku hakladım,
    Hak bendedir bende.

Hak ile birlikte
    yanar mı Yunus?
Doymayınca kalbi
    kanar mı Yunus?
Kırk yıllık hizmetten
    döner mi Yunus?
Onu sakladım,
    Hak bendedir bende.

Emr Resûl'den asla
    şaşamaz Yunus,
Tefrik zümresine
    düşemez Yunus.
Nur-u Muhammed'siz
    coşamaz Yunus,
Bir kucakladım,
    Hak bendedir bende.
