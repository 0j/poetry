# That Pearl

Long ago God grew a pearl
    nestled in the folds of space
And with one majestic gaze
    it shattered into all creation

God made the seven folds of earth
    spreading out the pearly dust
Made the seven folds of heaven
    from the pearl's swirling mist

Poured out all the seven seas
    from the dewdrops on the pearl
Sculpted seafoam into mountains
    made them fast upon the ground

Out of love for living creatures
    made Muhammed as their teacher
Loved the ones believing in him
    made Ali to help defend them

Heaven knows what work it took
    the wisdom in our holy book
Yunus drank in ecstasy
    from that pearl from the sea
