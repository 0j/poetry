# O Gevher

Hak bir gevher yarattı
    kendinin kudretinden,
Nazar kıldı gevhere,
    eridi heybetinden.

Yedi kat yer yarattı
    o gevherin tozundan,
Yedi kat gök yarattı
    o gevherin buğundan.

Yedi deniz yarattı
    o gevher damlasından,
Dağları muhkem kıldı
    o deniz köpüğünden.

Muhammed'i yarattı
    mahlukat şefkatinden,
Hem Ali'yi yarattı
    müminlere fazlından.

Kayıp işi kim bilir
    meğer Kuran ilminden,
Yunus içti esridi
    o gevher denizinden.
