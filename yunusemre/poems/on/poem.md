# Oh Nightingale

Are you a stranger to this place?
Oh nightingale why do you cry?
Did you grow tired and lose your way?
Oh nightingale why do you cry?

Have you crossed over desert sands?
Fought the mountain's snowy winds?
Did you get parted from your friend?
Oh nightingale why do you cry?

Alas, how cruel the fates can be
I wish that I could bring you peace
Is it your friend you long to see?
Oh nightingale why do you cry?

Did your nest and eggs all tumble?
Did it leave you crushed and humbled?
Did your friend leave you in trouble?
Oh nightingale why do you cry?

Once you summered in the gardens
Roses picked with dew still on them
Now you're desperate and downtrodden
Oh nightingale why do you cry?

From dreams I cried myself awake
And all my skin was stripped away
My heart was charred and still ablaze
Oh nightingale why do you cry?

Where did the one called Yunus go?
He followed God and drowned in love
And now the spring has come once more
Oh nightingale why do you cry?
