# Bülbül Hey

Sen burda garip mi geldin?
Niçin ağlarsın bülbül hey?
Yorulup iz mi yanıldın?
Niçin ağlarsın bülbül hey?

Karlı dağlardan mı aştın?
Derin ırmaklar mı geçtin?
Yarinden ayrı mı düştün?
Niçin ağlarsın bülbül hey?

Hey, ne yavuz inilersin,
Benim derdim yenilersin.
Dostu görmek mi dilersin?
Niçin ağlarsın bülbül hey?

Evin yuvan mı yıkıldı?
Namın ve arın mı kaldı?
Gurbette yarin mi kaldı?
Niçin ağlarsın bülbül hey?

Gulistanlarda yaylarsın,
Taze gülleri yeğlersin,
Yavlak zarılık eylersin,
Niçin ağlarsın bülbül hey?

Uykudan gözüm uyandı,
Uyandı kana boyandı.
Yandı sol yüreğim yandı,
Niçin ağlarsın bülbül hey?

N'oldu şu Yunus'a, n'oldu?
Aşkın deryasına daldı.
Yine baharistan oldu,
Niçin ağlarsın bülbül hey?
