# It Comes and Goes

Your love has caught my heart on fire,
    the burning came, the burning goes,
Your love has turned me upside down,
    the spinning came, the spinning goes
I broke my heart and poured out blessings,
    when I came to love the King
Your chain of love my neck embracing,
    bondage came, the bondage goes
The faithful hold to what you say,
    but never dare to meet your gaze,
My eyes upon the Friend's kind face,
    the looking came, the looking goes
I must not step into the fight,
    or let my heart upset my head
And yet my eyes are wet with blood,
    the crying came, the crying goes
Oh, the nightingale is sighing
    from a soul that aches with longing
Oh my own poor heart is breaking,
    parting came, the parting goes
Lover Yunus speaks these verses
    nightingales make shrieking noises
Here the Friend has planted roses
    fragrance came, the fragrance goes
