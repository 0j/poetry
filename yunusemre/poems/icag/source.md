# Allah

Aşkın odu ciğerimi,
    yaka geldi, yaka gider
Garip başım bu sevdayı,
    çeke geldi, çeke gider
Kâr etti firak canıma,
    âşık oldum ol Sultan'a
Aşk zencirin dost boynuma,
    taka geldi, taka gider
Şadıklar durur sözüne,
    gayrı görünmez gözüne
Bu gözlerim Dost yüzüne,
    baka geldi, baka gider
Arada olmasın naşı,
    onulmaz bağrımın başı
Gözlerimin kanlı yaşı,
    aka geldi, aka gider
Bülbül eder âh ü figan,
    hasretle yandı bu can
Benim gönülcüğüm ey can,
    çıka geldi, çıka gider
Aşık Yunus söyler bu sözleri,
    efgan eder bülbülleri
Dost bağçesinde gülleri,
    koka geldi, koka gider
