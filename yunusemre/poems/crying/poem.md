# Crying

One day I'll come before the Lord
On my knees and crying, crying
Death's messenger awaits my soul,
I hand it over crying, crying

The messenger will take my soul
All passed away, my life, my days
Condemned to moulder in a shroud
And all I'm good for's crying, crying

Pacing back and forth in darkness
Bloodshot eyes so wet and raw
One day into the shadowed grave
I will be lowered crying, crying

A waxen seal glues down my tongue
A weight is chained upon my arm:
The heavy book of all my sins
I hold it to me crying, crying

Lover Yunus, God means business
Sacrifice yourself to love
Faith is all our flesh can carry
I start praying, crying, crying
