# Bir Gün

Bir gün ol Hazret'e karşu
    varam ağlayu ağlayu
Azrail'e hem canımı
    verem ağlayu ağlayu

Çün Azrail ala canım,
    geçe benim ömrüm günüm
Kefen ola cümle tonum,
    gayem ağlayu ağlayu

Ben yürüyem yana yana,
    gözüm yaşı döne kana
Birgün şol karanlık sine,
    girem ağlayu ağlayu

Mühür uralar dilime,
    zincir uralar koluma
Amel defterim elime,
    alam ağlayu ağlayu

Aşık Yunus'un budur işi,
    yoluna fedadir başı
İman et bize yoldaşı,
    deyem ağlayu ağlayu
