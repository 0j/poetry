# Going Home

Those who came into this world
    should go again.
Here they're only guests,
    and they should go home someday.

Besides, before we left we promised
    to return and visit an old friend.
How long will we linger here?
    The time we've spent should be enough.

We'll arrive back home
    at just the right time for a visit.
When a traveller is sure of their destination,
    their heart should beat faster.

How can their heart not beat faster
    on the road to their homeland?
For people in love with life,
    this is the road they should travel on.

Why would a soul be planted here
    if it's not worthy of wisdom?
Why would the heart feel homesick
    if the tongue could explain it all?

I wonder what it's like back home...
If you could truly remember it,
    love would burst out of your head
    and set your heart to howling.

Yunus, tell us all about it now
    if you want to.
You're going home to marry your beloved,
    of course your heart is pounding.
