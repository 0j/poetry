# Misafirdir

Bu dünyaya gelen kişi,
    ahir yine gitse gerek,
Misafirdir vatanına,
    bir gün sefer etse gerek.

Vade kıldık o dost ile
    biz bu cihana gelmeden,
Nere kadar eğleniriz,
    o vademiz yetse gerek.

Biz de varırız o ile,
    nasıl ki vademiz gele,
Kişi varacağı yere,
    gönlünü berkitse gerek.

Gönül nice berkitmeye
    dost iline giden yola,
Aşık kişiler canını bu
    yola harc etse gerek.

Can niye ulaşır ise,
    akıl da ona harc olur,
Gönül neyi sever ise,
    dil onu şerh etse gerek.

Acep midir aşık kişi,
    maşuğunu zikrederse,
Aşk başından aşacağız
    gönlünü zâr etse gerek.

Yunus şimdi sever isen,
    ondan haber ver sen bize,
Aşığın odur nişanı,
    maşuğun söylese gerek.
