# What's Wrong With Me

Dervishes and brothers, hey!
I wonder what is wrong with me.
When people see my love drunk ways,
They wonder what is wrong with me.

No dervishes can be ashamed,
No lovers can be playing games,
No doctors cure what they can't name,
They wonder what is wrong with me.

Your ocean waves that crash and sweep,
My longing burned my heart so deep,
This hole in me makes people weep,
And wonder what is wrong with me.

My Lord, I dove into this pain,
Dove deep into your love's domain.
Against your waves I swim in vain,
And wonder what is wrong with me.

Lover Yunus, you dove again,
Dove straight into this mess you're in,
And entered truth, your only home,
I wonder what is wrong with me.
