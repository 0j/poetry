# Ne acep derdim var benim.

Ey dervişler, ey kardaşlar,
Ne acep derdim var benim.
Mecnun olmus der görenler,
Ne acep derdim var benim.

Dervis olan ar eylemez,
Aşık olan zar eylemez.
Hekimler timar eylemez,
Ne acep derdim var benim.

Deryanın mevci cağladı,
Hasret yüreğim dağladı.
Halim görenler ağladı,
Ne acep derdim var benim.

Derdine düştüm Mevla'nın,
Avarasıyım sevdanın.
Mevci yenilmez deryanın,
Ne acep derdim var benim.

Aşık Yunus düştün yine,
Düştün hemen aşk derdine.
Girdin hakikat yurduna,
Ne acep derdim var benim.
