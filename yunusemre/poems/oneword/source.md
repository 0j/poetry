# Bir Söz

Bu dem yüzüm süre duram,
    her dem ayım yeni doğar,
Her dem bayram olur bana,
    yazım kışım yeni bahar.

Bulutlar gölge edemez
    benim ayım ışığına,
Hem gedilmez doluluğu,
    nuru gökten yere doğar.

Onun nuru karanlığı
    sürer gönül hücresinden,
    -
O karanlık ile o nur
    bir hücreye nasıl sığar?

Evvel ay nice doğduysa
    ayrık dolanmadı asla,
Eksilmedi ömrü onun
    her kime kim kıldı nazar.

Ben ayımı yerde gördüm,
    ne isterim gökyüzünde,
Benüm yüzüm yerde gerek,
    bana rahmet yerden yağar.

Sözüm ay gün için değil,
    sevenlere bir söz yeter,
Sevdiğim söylemezsem,
    sevmek derdi beni boğar.

Onun vasfın söylersem
    halk maşuğu öğer sanır,
Hacet değil öğmek onu,
    kendi nurun kendi öğer.

N'ola Yunus sevdi ise,
    çoktur Hakk'ı seviciler,
Sevenleri gördü idi,
    onun için boyun eğer.
    -
