# One Word

Facing into this moment I linger,
    each moment my new moon rises,
    each moment I spend in celebrations,
    my summers and winters fresh as spring.

Clouds can't cast a shadow
    on the light of my moon.
Moonlight pours down from the heavens
    and will never run dry.

It shines into the darkness.
From its deep furrow,
    the heart sends up a tender shoot.
Swelling with darkness and moonlight,
    how can the ground hold it?

How many times the moon has risen,
    never straying from its path.
How many have gazed up at it,
    and its brightness never dims.

From the ground I looked up at my moon,
    and saw what I wanted in the sky.
But me, I need to face the earth,
    to me the earth is chanting blessings.

My verses are not for the sun and moon.
For lovers, one word is enough.
If I don't say it to my beloved,
    my love will pile up and bury me.

As soon as I say this word,
    everyone will know I'm in love.
Even if I keep quiet,
    they'll see it shining in my face.

So what if you're in love Yunus?
God has many lovers.
You've seen the lovers praying,
    so get down on your knees
    and join them.
