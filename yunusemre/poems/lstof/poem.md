# Life Slips Through our Fingers

We were given life in water
Life pours out between our fingers
Jewels will dazzle moneychangers
While the gold slips through their fingers

I am like a careless merchant
Never minding my accounting
How can I expect a profit?
Losses slipping through my fingers

But on this road, that's only wise
The dead can't carry merchandise
It's only naked souls can rise
And let the world slip through our fingers

On this road it's hard to tell
Both good and evil wear a veil
We stop and flirt with blasphemy while
Faith is slipping through our fingers

You build this castle round your soul
You're always working, stacking stones
But when the walls no longer hold,
"You" and "me" slip through our fingers

We walk through the crowded market
Nothing left inside our pockets
No more urge to buy, we've lost it
Time is slipping through our fingers

After all these many seasons
Oh how Yunus understands it
Lose your pride and you will ripen
Struggle slipping through your fingers
