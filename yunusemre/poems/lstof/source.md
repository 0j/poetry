# Canı Yağmaya Verdik

Niderüz biz hayat suyun,
    canı yağmaya verdik
Cevherleri sarraflara,
    madeni yağmaya verdik

Benim ol bezirgan kim,
    hiçbir assı gözetmedim
Çünki assıdan da geçtik,
    ziyana yağmaya verdik

Bu yolun arifleri
    geçirmezler her metaı
Biz şöyle üryan gideriz,
    cihanı yağmaya verdik

Küfür ile iman dahi,
    hicap imiş bu yolda
Safalaştık küfürle,
    imanı yağmaya verdik

Şenlik benlik olucağız,
    iş ikilikte kalır
Çıktık ikilik evinden,
    sen beni yağmaya verdik

Bu bizim pazarımızda,
    yokluk olur müşteri
Geçtik bitmez sağınçtan,
    zamanı yağmaya verdik

Pâyanli devr ü zaman,
    nice anlasın Yunus'u
Pâyasiz devre erdik,
    devamı yağmaya verdik
