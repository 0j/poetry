# I Came

I was commanded to this world
Knowing I had work to do, I came
I had no choice but to obey
And so to do my work, I came

Many times I came to this world
Came to saints and siezed their garments
Found the mighty and renowned
And boiling over in their souls, I came

With words of strength I broke their will
My fire set their hearts ablaze
The secrets of this world were mine
To show them to the common folk, I came

I made Idris like a tailor,
    cutting teachings from the scriptures,
    pinning, stitching into garments
I taught Seth to be a weaver,
    spinning threads of holy law,
    on his loom they turned to scriptures
David sang resplendent psalms,
    when he cried out to me, I came

I came to love the moon's bright blade
Her keen edge dripping honeyed moonlight
I lost myself in her dark eye
To the shadow nestled
    in the crescent moon, I came

When Moses went upon the mountain,
    as the sacrificial ram,
    to walk beside him meek, I came
When Ali unsheathed his sword,
    through the broken city gates,
    to fight beside him brave, I came

Like the ocean to the shore
Like the bucket to the well
Like prayer to Jesus on the tongue
Thus to do my work, I came

In the full moon when it rose
In the cloud hung from the sky
In the raindrop when it fell
In the sunbeam when it shone, I came

By rumors told to passersby
By inspirations shown to pilgrims
By revelations made to seekers
By hidden signs in dreams, I came

Mine the only cure for suffering
Mine the art of sculpting flesh
Mine the lineage of Moses
And to meet him on the mount, I came

Now my path has come to you
Who have learned to call me Truth
When your tongue rang with my name
You spoke of me, you thought of me, I came
