# Geldim

Beni buraya yollayan
    bilir ben ne işe geldim,
Kararım yok bu dünyada,
    giderim yumuşa geldim.

Dünyaya çok gelip gittim,
    erenler eteğin tuttum,
Kudret ününü işittim,
    kaynayarak coşa geldim.

Sert söz ile gönül yıktım,
    od oldum canları yaktım,
Sırrımı aleme çaktım,
    bu halka temaşa geldim.

Ben bir İdris gibi terzi,
    -
    -
Şit oldum dokudum bezi,
    -
    -
Davut'un görklü avazı,
    ah edip nalişe geldim.

Aşık oldum şu ay yüze,
    nisar oldum bal ağıza,
Hayran baktım kara göze,
    siyah olup kaşa geldim.
    -

Musa oldum Tur'a vardım,
    koç olup kurbana geldim,
    -
Ali olup kılıç saldım,
    meydana güreşe geldim.
    -

Deniz kenarında ova,
    kuyuya işleyen kova,
İsa ağzındaki dua
    olarak ben işe geldim.

Ay olup aleme doğdum,
    bulut olup göğe ağdım,
Yağmur olup yere yağdım,
    nur olup güneşe geldim.

Kılükaldan geçenlere,
    yolda gözün açanlara,
Anlayarak seçenlere,
    vaka olup düşe geldim.

Benim dertliler dermanı,
    benim o marifet kanı,
Benim Musa'nın İmran'ı,
    Tur dağından aşa geldim.

Yolum sana oldu durak,
    dersini söyleyendir Hak,
dilinde Hak olup
    dile düşe geldim.
