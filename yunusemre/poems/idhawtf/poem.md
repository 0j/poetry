# I Don't Have any Wish to Fly

My eyes are for beholding you
My hands for reaching out to you
Today my soul goes seeking you
I hope tomorrow finding you

Today my soul goes seeking you
Tomorrow you'll give me my voice
Your heaven's what I'm longing for
I don't have any wish to fly

To fly to heaven, so they say
To that reward the faithful chase
With virgins lounging round the place
I have no lust for their embrace

You gave me so much already
(Doubly blessed with son and daughter)
Even this desire has left me
Let me run away with you

Give devotees your memory
It's you I want, it's you I need
These days I don't even care
To sit here in my rocking chair

I've been longing for your presence
As you long to show your essence
In your work there is no cruelty
Making this world as it should be
