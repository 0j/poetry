# Arzum Yoktur Uçmak İçin

Gözüm seni görmek için
Elim sana ermek için
Bugün canım yolda koyam
Yarın seni bulmak için

Bugün canım yolda koyam
Yarın avazım veresin
Arzeyleme uçmağını
Hiç arzum yok uçmak için

Uçmak uçmağın dediğin
Müminleri yeltediğin
Bir ev ile birkaç huri
Hevesim yok koçmak için

Bunda dahi verdin bize
(Oğul ve kız çifte helâl)
Ondan dahi geçti arzum
Azmim sana kaçmak için

Sofulara ver sen anı
Bana seni gerek seni
Benim hiç hevesim yoktur
Şol bir ev ü çardak için

Yunus hasrettir sana
Hasretin göstergil ana
İşin zulum değil ise
Dad eylegil varmak için
