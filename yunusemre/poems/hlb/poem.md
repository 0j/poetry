# Hey Loverboy

Hey loverboy, head in the clouds
Hey, now and then why not look down?
See graceful flowers on the ground
Here and gone within a season

Dressed in lovely summer gowns
They sway and bend among their kind
Now go and ask a craftsman whether
He could make just one so fine

From each flower's thousand graces
Learn to pray with upturned faces
From each bird whose sweet voice rises
Learn the way to sing God's praises

Study on God's might and power
Making ripe each fruit and flower
Surely in a lifetime we can
Understand the colors turning

Colors blooming day by day
To fall to earth and rot away
In this turning there's a lesson
Might be wise to pay attention

If you understood it more
Oh grief would shake you to the core
Such gut-deep grief to set you spinning
Shedding all your sins and winnings

What did you come here to do?
What will knowing things amount to?
When you've finished you will die
And love won't get you out alive

We all know people come and go
God puts us here and takes us back
Those drinking up love's honeyed wine
And everyone who hears these lines

Hey Yunus, these words smell like earth
You quit your playing in the dirt
If your words from God descended
There could be no evil in them
