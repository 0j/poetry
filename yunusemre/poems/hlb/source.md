# Ey Aşk Eri

Ey aşk eri aç gözünü,
    yeryüzüne eyle nazar,
Gör bu latif çiçekleri,
    bezenerek geldi geçer.

Bunlar yazın bezenirken,
    dosttan yana uzanırken,
Bir sor ahi sen bunlara,
    nereyedir azm-i sefer.

Her bir çiçek bin naz ile,
    öğer Hakk'ı niyaz ile,
Her kuş hoş bir avaz ile,
    o padişahı zikreder.

Öğer onun kadirliğin,
    her bir işe hazırlığın,
İlla ömür kasırlığın,
    anacağız rengi döner.

Rengi döner günden güne,
    toprağa dökülür yine,
İbret olur anlayana,
    bu ibreti arif duyar.

Bu sırrı ger duyaydın,
    ya bu gamı yiyeydin.
Yerinde eriyeydin,
    gideydi senden kar ve kir.

Ne gelmeğin gelmek olur,
    ne bilmeğin bilmek olur,
Son menzilin ölmek olur,
    duymadın aşktan bir eser.

Bildik gelen geçer imiş,
    konan geri göçer imiş.
Aşk şerbetin içer imiş,
    her kim bu manadan duyar.

Yunus bu sözleri kogil,
    kendözünden elin yugil,
Senden ne gele bir değil,
    çün Hak'tan gelir hayır şer.
