# If That's the Way

Let it happen, if that's the way
Be one with God if that's the way
The sea of love has flowed with blood
Jump right in if that's the way
It drowns its swimmers so they say
Let it kill you if that's the way
The afterlife will never end
Stay forever if that's the way
One day your well will fill with dirt
Let it fill if that's the way
Some folks sit upon high horses
Let them ride if that's the way
