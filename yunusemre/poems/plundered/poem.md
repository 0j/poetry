# Plundered

I have found the soul of souls
Let this soul of mine be plundered
Gone beyond all gains and losses
Let this shop of mine be plundered

I gave up my selfish ways
Tore the veil from off my face
Fell into the Friend's embrace
Let these doubts of mine be plundered

I gave up on being me
The Friend devoured my property
And I became a landless gypsy
Let my worldly goods be plundered

I got tired of being a loner
Dressed up fancy like a lover
Got mixed up with tavern brawlers
Let this strength of mine be plundered

Lonely creatures searched in fright
The Friend came to us in the night
To fill our ruined hearts with light
Let this world of mine be plundered

I got tired of endless cravings
Spinning through the wheel of seasons
I have found the queen of gardens
Let my little plot be plundered

A friend and I got in a fight
I ran back to apologize
Gave in and took love's good advice
Let my own advice be plundered

Yunus you've been talking sweet,
What honeyed candy did you eat?
I've found the most delicious honey
Let this hive of mine be plundered
