# Yağma Olsun

Canlar canını buldum,
    bu canım yağma olsun,
Assı ziyandan geçtim,
    dükkanım yağma olsun.
Ben benliğimden geçtim,
    gözüm hicabın açtım,
Dost vaslına ulaştım,
    gümanım yağma olsun.

Benden benliğim gitti,
    hep mülkümü dost yuttu,
Lamekan kavmi oldum,
    mekanım yağma olsun.

İkilikten usandım,
    aşk donunu donandım,
Derdin hanına kandım,
    dermanım yağma olsun.

Varlık çün sefer kıldı,
    ondan dost bize geldi,
Viran gönül nur doldu,
    cihanım yağma olsun.

Geçtim bitmez sağınçtan,
    usandım yazdan, kıştan,
Bostanlar başın buldum,
    bostanım yağma olsun.

Taalluktan üzüştüm,
    o dosttan yana uçtum,
Aşk divanına düştüm,
    divanım yağma olsun.

Yunus ne hoş demişsin,
    balla şeker yemişsin,
Ballar balını buldum,
    kovanım yağma olsun.
