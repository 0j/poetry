# You Can't

Dervishood has said to me
You can't become a dervish
Even riding on my back
You can't become a dervish

Openhearted, feeling deep
A dervish must prepare to weep
Must be gentle as a sheep
You can't become a dervish

Must be handless when attacked
Must be cussed and not talk back
Must give up the will to act
You can't become a dervish

Run your mouth with empty praise
All those words get in your way
Yelling when they won't obey
You can't become a dervish

On this path, if yelling helped
Then Muhammed would have yelled
You've been known to yell yourself
You can't become a dervish

If you fail to get it right
If you fail to find a guide
If destiny's not on your side
You can't become a dervish

Dervish Yunus come here now
Dive into the sea right now
If you don't dive in the sea
You can't become a dervish
