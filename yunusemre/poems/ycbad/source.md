# Sen Derviş Olamazsın

Dervişlik der ki bana
Sen derviş olamazsın
Gel ne deveyim sana
Sen derviş olamazsın

Derviş bağrı baş gerek
Gözü dolu yaş gerek
Koyundan yavaş gerek
Sen derviş olamazsın

Döğene elsiz gerek
Söğene dilsiz gerek
Derviş gönülsüz gerek
Sen derviş olamazsın

Dilin ile şakırsın
Çok mâniler okursun
Vara yoğa kakırsın
Sen derviş olamazsın

Kakımak varmışsa ger
Muhammed de kakırdı
Bu kakımak sende var
Sen derviş olamazsın

Doğruya varmayınca
Mürşide yetmeyince
Hakk nasip etmeyince
Sen derviş olamazsın

Derviş Yunus gel imdi
Ummanlara dal imdi
Ummana dalmayınca
Sen derviş olamazsın
