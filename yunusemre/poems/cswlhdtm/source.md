# Gel Gör Beni Aşk Neyledi

Gönlüm düştü bu sevdaya
Gel gör beni aşk neyledi
Başımı verdim kavgaya
Gel gör beni aşk neyledi

Ben ağlarım yana yana
Aşk boyadı beni kana
Ne âkilim ne divane
Gel gör beni aşk neyledi

Mecnun oluben yürürüm
Dostu düşümde görürüm
Uyanır melûl olurum
Gel gör beni aşk neyledi

Aşkın beni mest eyledi
Aldı gönlüm hasteyledi
Öldürmeğe kast eyledi
Gel gör beni aşk neyledi

Gâh eserim yeller gibi
Gâh tozarım yollar gibi
Gâh akarım seller gibi
Gel gör beni aşk neyledi

Akar suların çağlarım
Dertli yüreğim dağlarım
Yârım için ben ağlarım
Gel gör beni aşk neyledi

Benzim sarı, gözlerim yaş
Bağrım pare, ciğerim baş
Halden bilen dertli kardaş
Gel gör beni aşk neyledi

Miskin Yunus biçâreyim
Baştan ayağa yâreyim
Dost elinden âvâreyim
Gel gör beni aşk neyledi
