# Come See

My reckless heart dreamed up this love
Come see what love has done to me
My head thinks twice and won't give up
Come see what love has done to me

My tears roll down in tracks of sadness
Love has painted me with blood
Is this wisdom, is this madness
Come see what love's done to me

I walk and walk despairingly
I dream the Friend is there with me
Wake up and fall upon my knees
Come see what love has done to me

Your love has cast a spell on me
A sickness come to dwell in me
I think it plans on killing me
Come see what love has done to me

Sometimes like the winds I blow
Sometimes dusty as the roads
Sometimes like the floods I flow
Come see what love has done to me

Your waters flow, eternal fountains
My heart aching for my mountains
Feet that walk in two directions
Come see what love's done to me

My face is pale, my eyes are wet
My shattered heart, my guts upset
My worried friends begin to fret
Come see what love has done to me

Poor old Yunus, tired and poor
From head to foot an open sore
Wandered far from your friend's door
Come see what love has done to me
