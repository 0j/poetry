# Nikabı Yüzünden Bırak

Kerem ile bir beri bak,
    nikabı yüzünden bırak,
Sen ayın on dördü müsün,
    balkırır o yüz ve yanak.
Şu bal ağızdan keleci
    yüz bin şekerden tatlıdır,
    -
Söyler olursa bu dilin.
    deprenir olsa o dudak.
Otuz iki inci bitmiş
    mercan içinde ey canım,
Kıymeti ala inciden,
    aklığı da inciden ak.
Yüzüne karşı bu güneş,
    bir dem gelerek duramaz,
Gelip kaşından küçük ay
    her dem okuyalı sebak.
Seni gören tek pervane,
    nicesi oda düşmesin,
İki yanadın çün durur
    o iki şûleli çerak.
Bu aşkın selasilinde,
    bu zencire kim ki düşse,
Azat olmak istemez o,
    olsa da vücudu toprak.
Dil nice vasfetsin senin
    hüsnün ile hulkunu ki,
Hüsnünü hoş eylesin Hak,
    dur kötü gözlerden ırak.
İşittim ki boyun senin
    serviden de ala imiş,
Dahi gözümle görmeden
    dal boyunu sevdi kulak.
Yûnus Hak tecellîsini
    senin nur yüzünde gördü,
Çare mi var ayrılığ,
çünkü sende göründü Hak.
