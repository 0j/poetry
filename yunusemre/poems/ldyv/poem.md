# Let Down Thy Veil

Let down thy veil, allow me just one glimpse
I see the full moon shining from thy cheek
And from thy lips the honeyed light that drips
And from thy smile more sweet
    than sugar's sweet
Comes more than any mortal tongue could say
And part thy lips, among the coral beds
A string of pearls brighter than the day
That glimpse, that instant will not leave my head
And draws me like a moth to candle flames
I circle round thy light, I'm trapped, I'm hurt
I beg thee, don't release me from these chains
Without thy light my body's only dirt
Oh Yunus, join the flame, I'll set thee free
The light thou saw reflects God's light in thee
