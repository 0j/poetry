# ECEL OKU ERDİ CANA

Ecel oku erdi cana,
    kafle göçtü dur diyeler,
Gafil olan yolda kalır,
    hazırlığı gör diyeler.

Anmaz idim o sultanı,
    ne işe gönderdi beni,
Emanet verilen canı,
    sahibine ver diyeler.

Çün can bedenden çekile,
    şu elif kamet büküle,
Gözünden gevher döküle,
    gel merteben sor diyeler.

Şöyle yürürken naz ile,
    adın deftere yazıla,
Kara yerde ev düzile,
    gel günahkar gir diyeler.

Kabrin sual eder sana,
    hani armağının bana,
Armağansız gelen burda,
    yılan çiyan yer diyeler.

Münafıkın aklı şaşa,
    Rabbim bilmem diye haşa,
Kabir dar ola kavuşa,
    Hak buyurdu kır diyeler.

Mümin olan gele dile,
    cevap vere güle güle,
Cennet'ten huriler gele,
    kabrin dolu nur diyeler.

Yunus sabret bu mihnete,
    bir gün eresin rahata,
Yine Hakkın lütfu yete,
    gel Cennet'e gir diyeler.
