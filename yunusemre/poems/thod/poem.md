# The Hour of Death

Now your hour of death has come,
the rest walk on without a word.
"How careless to get left behind,"
those who came prepared are saying.

*Don't you remember*, says the Ruler,
*what job I sent you here to do?*
*I trusted you with life and soul,*
*and all you bring me is this talking.*

Your soul is yanked away from flesh,
your upright I slumps down and falls,
the jewels within your eyes spill out.
"How did it come to this?" you say.

*Well, you walked through life so prideful,*
*your name was written in the book.*
*This house of earth is low and level,*
*come now, sinner, come in and talk.*

"Wait, is this my grave?" you say,
"Where are the rewards I looked for?"
*Did you show up empty-handed?*
*Creeping, crawling, grasping, talking?*

Your cleverness led you astray;
without my Lord your evil tongue
has sent you to a shallow grave
in this place of desolation.

If you'd been among the faithful,
you could say "goodbye, goodbye,"
and angels would fly down to earth
to fill your grave with words of glory.

Yunus stop it with this worry,
the day will come for you to rest.
God will give you one more blessing:
*Come to heaven, come in and talk.*
