# Bana Seni Gerek Seni

Aşkın aldı benden beni,
    bana seni gerek seni
Ben yaparım dün ü günu,
    bana seni gerek seni
Ne varlığa sevinirim,
    ne yokluğa yerinirim
Aşkın ile avunurum,
    bana seni gerek seni
Aşkın aşıkları öldürür,
    aşk denizine daldırır
Tecelli ile doldurur,
    bana seni gerek seni
Aşkın şarabından içem,
    Mecnun olup dağa düşem
Sensin dün ü gün endişem,
    bana seni gerek seni
Sofilere, sohbet gerek,
    Ahilere Ahret gerek
Mecnunlara Leyli gerek,
    bana seni gerek seni
Eğer beni öldüreler,
    külüm göğe savuralar
Toprağım anda çağıra,
    bana seni gerek seni
Cennet Cennet dedikleri,
    birkaç köşkle birkaç huri
İsteyene ver sen anı,
    bana seni gerek seni
Yunus'dürür benim adım,
    gün geçtikçe artar odum
İki cihanda maksudum,
    bana seni gerek seni
