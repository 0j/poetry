# You're the One for Me

Your love welled up from deep within,
    you're the one for me I need you
Day and night it's spilling over,
    you're the one for me I need you
What wealth could ever make me glad,
    what poverty could make me sad
I'm too distracted with your love,
    you're the one for me I need you
Your love is even known to kill
    the fools who dive into your sea
With you their drowning lungs will fill,
    you're the one for me I need you
Your love's like wine, and I can't stop,
    passed out drunk on a mountaintop
Asleep, awake, my only thought
    is you're the one for me I need you
Mystics need some talk and laughter,
    workmen need the sweet hereafter
Star-crossed lovers need each other,
    you're the one for me I need you
And if your love should do me in,
    scatter my ashes to the wind
What's left of me will cry out then:
    you're the one for me I need you
Heaven, Heaven, say the churchmen,
    have some mansions, have some virgins
They can have them if they want them
    you're the one for me I need you
Yunus is what I've been named,
    each day that passes fans my flame,
In heaven and earth I want the same,
    you're the one for me I need you
