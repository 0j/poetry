# Invocation

With the mountains, with the stones,
    let me call thy name my Lord
With the birds at dusk and dawn,
    let me call thy name my Lord
With the fish in waters deep,
    with the wilderness's deer,
With the sages praying "dear God,"
    let me call thy name my Lord
With the radiant face of Christ,
    with Moses climbing Mount Sinai
With staff in hand beneath the sky,
    let me call thy name my Lord
With the burdened sighs of Job,
    with the crying eyes of Jacob,
With Muhammad the beloved
    let me call thy name my Lord
With praise for all that you provide,
    with grace to bear what you decide,
With ceaseless prayer to burn my pride,
    let me call thy name my Lord
I have known the world's ways,
    I've left behind what rumors say,
Clear head, bare feet to feel the way,
    let me call thy name my Lord
Yunus listens to the tales
    of mourning doves and nightingales,
Truth is seeking servants still,
    let me call thy name my Lord
