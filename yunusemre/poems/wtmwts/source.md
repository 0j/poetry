# Dağlar İle, Taşlar İle

Dağlar ile, taşlar ile
    çağırayım Mevlam seni
Seherlerde kuşlar ile,
    çağırayım Mevlam seni
Sular dibinde mahi ile,
    sahalarda ahu ile
Abdal olup yahu ile,
    çağırayım Mevlam seni
Gökyüzünde İsa ile,
    Tur dağında Musa ile
Elimdeki asâ ile,
    çağırayım Mevlam seni
Derdi öküş Eyyub ile,
    gözü yaşlı Yakub ile
Ol Muhammed-i mahbub ile
    çağırayım Mevlam seni
Hamd ü şükrullah ile,
    vasf-i Kulhüvallah ile
Daima zikrullah ile,
    çağırayım Mevlam seni
Bilmişim dünya halini,
    terk ettim kıyl ü kalini
Baş açık, ayak yalını,
    çağırayım Mevlam seni
Yunus okur diller ile,
    ol kumru bülbüller ile
Hakkı seven kullar ile,
    çağırayım Mevlam seni
