# I Need It Back

Your love was raining on my heart
I need those raindrops back again
A flame has gone out in my soul
I need that burning back again

Back when I had fire and rain
I fed the stove, I reaped the grain
The sun plowed wrinkles on my skin
I need that sunlight back again

Down what strange paths my life has led
My heart hung heavy with regrets
And leaked and left me soaking wet
I need that bleeding back again

I had eyes so raw with weeping
Knees so bruised with constant kneeling
Looked up to the Friend, appealing
Bring that feeling back again

Once I knew I came to serve you
Knew the path of love and virtue
Knew my longing to deserve you
Bring that knowing back again

Once my heart cried out for yours
Each moment we were parted for
I beat my face against your door
I need to be like that again

Yunus once had crazy notions
Swam the sea and learned its motions
Found a pearl beneath the ocean
Dive and find that pearl again
