# Mine

The faith in shrine and idol, mine
Men broken on the wheel are mine
The passing cloud, the shooting star
The rain that turns to snow are mine

Summer-bringer paints the colors
Sows the seeds in hearts of lovers
Proud forefathers and foremothers
They who live to serve are mine

Birdsong when the thunder passes
Heads that swell with shouts and curses
Fattening among the grasses
Crawls a viper that is mine

To Hamza from the farthest shores
Came footsoldiers for holy wars
To topple scores of pagan lords
His cunning and his zeal are mine

Winding flesh on bony spindle
Knitting skin, with life I kindle
Wisdom sleeping in the cradle
Strength that flows from breasts is mine

Well my true love, since you've come
I will show you where it starts
The road that leads into the heart
They who stay and never part are mine

Calling mine the skies and valleys
Calling mine the west and east
And summoning the seven seas
My name is Yunus and the ocean's mine
