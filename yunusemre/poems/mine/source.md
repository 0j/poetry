# Benim

Kabe ve büt iman benim,
    çark vurup da dönen benim,
Bulut olup göğe ağan,
    yağmur olup yağan benim.

Yaz yaratıp yer donatan,
    gönlümüz evi hanedan,
Hoşnut ata ve anadan,
    kulluk kadrin bilen benim.

Yıldırım olup şakıyan,
    kakıyıp nefsin dokuyan,
Yerin altında berkiyen
    şu ağılı yılan benim.

Hamza'yı Kaf'tan aşıran,
    elin ayağın şişiren,
Çokları tahttan düşüren
    hikmet ıssı sultan benim.

Et ve deri kemik çatan,
    yaratıp da diri tutan,
Hikmet beşiğinde yatan,
    kudret sütün emen benim.

Gerçek aşık gelsin beri,
    gösterelim doğru yolu,
Makam olur gönül şarı,
    ırılmayıp duran benim.

Yere göğü benim diyen,
    mağrip maşrık benim diyen,
Denizlere gel çağıran,
    adım Yunus, umman benim.
