# Bahar

Gitti bu kış zulmeti,
    geldi bahar yaz ile,
Yeni nebatlar bitti,
    dalgalandı naz ile.
Yine mergizar oldu,
    uş yine gülzar oldu,
Ter nağme düzer oldu,
    musikide saz ile.
Hoş haber geldi dosttan,
    yarattı bağ ve bostan,
İlm okur hezar destan
    bülbülleri raz ile.
Kim görmüştür baykuşun
    gülistana girdiğin,
Leylekler zikredemez
    bir latif avaz ile.
Ya nice saklar isen
    dürdane gevher olmaz,
Keklik keklikle uçar,
    hemişe baz baz ile.
El kuşu elden ele,
    gül kuşu gülden güle,
Baykuş virane sever,
    şahinler pervaz ile.
Nerde ki bir gövde var,
    akbaba orda üşer,
Tutiler evde şeker
    bulurlar kafes ile.
Her şahsın kendi tuşu,
    kendine tuş eyledi,
Sadıklar ikrar ile,
    sofular namaz ile.
Cahil, münafık, münkir,
    cümle aklına şakir,
Aşıklar didar sever,
    arifler niyaz ile.
Dervişlik dedikleri
    dilde haber değildir,
Hak ile Hak olana
    orda menzil düzüle.
Ben dervişim diyenler,
    yalan dava kılanlar,
Yarın Hak didarını
    göremezler göz ile.
İlmi amel ne fayda
    bir gönül yıktın ise,
Arif gönül yaptığı
    beraber hicaz ile.
Ulu divan kurula,
    orda kulluk sorula,
Bin tekebbür vermeye
    bir garip nevaz ile.
Eğriler eğri ile,
    doğrular doğru ile,
Yalan yalanı sever,
    gammazlar gammaz ile.
Kimi dükkandan bakar,
    kimi hoşluklar sever,
Kimi bir pula muhtaç,
    kimisi canbaz ile.
Kula nasip değecek
    sultan elden alamaz,
Zülkarneyn n‘eyledi ya,
    Hızır ve İlyas ile.
Görmez misin Edhem'i
    tahtını terk eyledi,
Hak katında has oldu,
    bir eski palas ile.
Bu dünyaya inanma,
    dünyayı benim sanma,
Niceler benim demiş
    giderler ham bez ile.
Aşk yağmuru damlası
    gönül göğünden damlar,
Sevgi yeli götürür
    yağmuru ayaz ile.
Yunus şimdi gam yeme,
    n'idem, ne kılam deme,
Gelir kişi başına
    ezelden ne yazıla.
