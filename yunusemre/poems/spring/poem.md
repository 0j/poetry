# Spring

Dead and gone the dark of winter,
    summer's chasing after spring
Herbs have sprouted fresh and tender,
    playful winds are lingering
Pastures green and lush again,
    beds of roses blush again
Fragrant tunes composing in
    the music only reeds can sing
Pleasant rumours from a friend
    buzzing in the grove and garden
Secrets lovers comprehend
    the nightingales are warbling
Who has seen the wise old owl
    in a garden tame and tended
Noble storks chant raspingly
    their voices aren't the sweetest thing
Every stem is strung with pearls
    that a jeweler wouldn't buy
Partridges fly side by side,
    they only think of eating
Tame birds perch upon the hand,
    nightingales in roses land
Owls prefer the lonely ruins,
    falcons rise and spread their wings
Where there is a lone dead tree,
    vultures crowd like bourgeoisie
Parrots with a taste for sweets
    will find a cage to perch repeating
Every person has their key,
    the key that they were given
Believers have their catechisms,
    pious folks have praying
Doubters, fools, and hypocrites,
    thankful when they think of it
Lovers like to visit,
    and the clerics like their pleading
Dervishes should all know better,
    tongues are not for idle chatter
Best to put the house in order,
    Truth with Truth be gathering
"I'm a dervish," some will say,
    but they're making idle claims
If tomorrow's Judgement Day
    it's not God's face they're seeing
Being good won't get you far
    if you ever break a heart
To the holy land depart
    and maybe you'll find healing
Go to the almighty's house and
    ask if you can be a servant
There a sniffling beggar can
    be showered with a thousand blessings
Good folks know the world is good,
    crooks think that it's crooked
Liars can't believe the truth,
    tattletales like tattling
Some folks like to browse in shops,
    some like having nicer things,
Some need just a bit more cash,
    some their lives are frittering
Rich rewards of serving God,
    even kings can't take away
Nor demons at the end of days,
    with all the saints defending
Can't you see what Adhem saw
    who left his throne to be a saint?
In God's sight your stubborn pride
    is like a filthy rag you'd fling
Don't believe this fleeting world,
    don't believe it's yours to keep
Many who have said "it's mine"
    are shrouded corpses rotting
Drops of love fall from God's sky,
    rain to nourish thirsty hearts
Love's warm wind will blow away
    the bitter winter's frosty sting
Yunus, time to leave your sorrow
    you know nothing of tomorrow
We are born from the eternal,
    who can say what fate will bring?
