Yunus wrote a lot of poems, hundreds of them, so many that I haven't even
looked at them all, and here I've only been able to translate a few.
I selected them mostly by looking for imagery and phrases that called
out to me, because I have to be excited about what I'm doing for my work
to be any good. Some poems dropped a lot of names, and I skipped them
because most readers wouldn't know the names. Others seemed to be
referencing doctrinal disputes, and I skipped them because after
all these years, the point is either no longer disputed or highly
controversial. The ones I was sorry to skip over were the riddle poems,
which are fascinating, but it's just too hard for me to tell whether
I've got the meaning right, and even if I understood, it might depend
on deep wordplay that can't be translated. *Let There Be Separation*
has a little bit of that flavor. In the end, I've included
the poems I stumbled across and liked, and I hope you like them too.
If you're a fan of Yunus Emre and there's a poem you wish were in this book,
please let me know and maybe we can make a second volume.

I wish I could direct you straight to the source material for each poem,
but unfortunately it's not easy to find an authoritative source that's
easily accessible without ordering a book from Turkey.
These poems were originally circulated by word of mouth, so they don't have
official titles, and they appear in various versions depending on
which compilation you look at. Over the years, some of the poems
may have been changed, added to, or even composed by someone else in
Yunus's style. The sources I've translated from have themselves been
translated into more modern Turkish, which is another place for variations
to creep in. I've decided to embrace the spirit of folk poetry,
which is as messy as history itself, and not worry too much about it.
But in case you do want to search for original text,
the following list gives you a Turkish phrase that's characteristic
of each poem, usually either the refrain or the first line.

**Come See**: ~gel gör beni aşk neyledi~

**Crying**: ~bir gün ol Hazret'e karşu~

**Hey Loverboy**: ~ey aşk eri aç gözünü~

**I Came**: ~beni buraya yollayan~

**I Don’t Have any Wish to Fly**: ~gözüm seni görmek için~

**I Need It Back**: ~gerek şimden geri~

**If That's the Way**: ~n'olur ise ko ki olsun n'olusar~

**Inside of Me**: ~Hak bendedir bende~

**Invocation**: ~dağlar ile, taşlar ile~

**It Comes and Goes**: ~aşkın odu ciğerimi~

**Going Home**: ~bu dünyaya gelen kişi~

**Let Down Thy Veil**: ~nikabı yüzünden bırak~

**Let There Be Separation**: ~ko ikiliği~

**Life Slips Through our Fingers**: ~canı yağmaya verdik~

**Lying Down**: ~sabah mezarlığa vardım~

**Mine**: ~Kabe ve büt iman benim~

**Oh Nightingale**: ~niçin ağlarsın bülbül hey~

**One Word**: ~bu dem yüzüm süre duram~

**Plundered**: ~bu canım yağma olsun~

**Poison**: ~dost ilinin haberin desem~

**Spring**: ~gitti bu kış zulmeti, geldi bahar yaz ile~

**That Pearl**: ~Hak bir gevher yarattı~

**The Hour of Death**: ~ecel oku erdi cana~

**What's Wrong With Me**: ~ne acep derdim var benim~

**You Can’t**: ~sen derviş olamazsın~

**You’re the One for Me**: ~bana seni gerek seni~
