Languages and cultures are profoundly different, and this means
there's no such thing as a perfect translation. Instead, a translator has to
make sacrifices and set priorities that help decide what to sacrifice.
Some people prefer a literal rendering, some like a learned one,
some like a beautiful one. My priorities are:

1. Be faithful to the underlying message. This is the essence of what Yunus was
   trying to get across and the reason he wrote poems at all. If I lose his
   message or distort it too much, I've failed. Unfortunately I'm limited by my
   ability to understand, but that's an inescapable part of the human condition.
2. Speak plainly and naturally. This makes the poems accessible to a wider
   audience, and also makes them fluid enough to recite or read aloud, as
   people did at the time of their writing. I've tried to avoid fancy words and
   tricks like poetic inversion, although in some cases I just couldn't help
   myself.
3. Preserve the rhythm and rhyme. The poems are wonderfully lyrical in the
   original Turkish, and I wanted to keep as much of that sound as I could.
   This type of structured verse has become less common in modern poetry,
   even though most people still love rhythm and rhyme
   (tune into any pop radio station for proof). I think this
   might partly explain the waning cultural importance of poetry.
4. Preserve the specific imagery. If I can satisfy all of the other priorities,
   I've tried to keep the surface meaning the same as the original, and this
   makes my job easier because Yunus's imagery is often inventive and memorable.

The poems refer to symbolism, scripture, legends, and spiritual figures
that English-speaking readers are unlikely to be familiar with. I've resolved
these in the following ways:

1. By adding a few words to give the reader some context (e.g., "Adhem...who left
   his throne to be a saint"). This is good because even without looking at the
   original, readers can easily dive deeper and learn about the people and ideas
   being referred to. The downside is that it uses up precious syllables
   and isn't always possible without breaking the form or the flow. When
   the references are to fairly well-known figures and ideas from Judaism,
   Christianity, and Islam, I've mostly left them as-is, only changing
   the names to their English-language or Arabic equivalents.
2. By translating into a parallel reference from the western canon (e.g., Layla
   and Majnun become "star-crossed lovers" in reference	to Romeo and Juliet).
   These correspondences are rarely very exact, so I've used this method sparingly.
3. By translating the meaning as I understand it (e.g., for a reference to Dhu
   al-Qarnayn saying  "ya," I interpreted this to be about him releasing Gog and
   Magog, and translated it into "demons at the end of days"). Here I've decided
   to sacrifice specificity to make the poems more widely accessible. I've also
   made sultans into kings, Sufis into mystics, and so on, both to make it easier
   for readers and easier for me, since the more traditional English words are
   easier to fit into a poem's structure. Of course, this makes the poems sound
   less exotic, but I think exoticism comes out of a mistaken fascination with
   the surface and not the substance. In Yunus's time, listeners would not
   have found anything exotic in these poems; it was just the ordinary world
   to them.

One word I haven't translated is "dervish," because the only close equivalent
in English is "mendicant friar," which is really hard to fit into verse,
and also doesn't convey that dervishes could get married and hold ordinary
jobs. We have some other words like "mystic," "ascetic," and "monk," but
I feel like each only captures part of what it means to be a dervish.
Hopefully you'll either already be familiar with the word,
do some research to become familiar with it, or just get the idea from the
poems themselves. Dervishood encompasses a number of related spiritual paths,
but I think that what Yunus meant by it would have included simple living
or voluntary poverty, devotional practices like meditation, prayer,
and fasting, and renunciation of the self and all worldly goals.

Indo-European languages like English divide the world into he, she, and it.
Those of us who grew up speaking these languages are so entrenched in that way
of looking at things that we rarely notice it, except when learning
another Indo-European language with a different set of rules. Turkish and
related languages are in a different family and mostly don't draw a gramatical
distinction between male and female, living and nonliving. This leaves a certain
ambiguity, and I've tried to preserve that ambiguity where possible because
I feel it supports the universal nature of Yunus's message.
