Thanks to God for this wonderful world.
Thanks to Yunus Emre for channeling God's love and humanity's struggles.
Thanks to my family, my friends, and all my many teachers for raising
    and nurturing my body, mind, and spirit.
Thanks to Tyler for helping me up from a dark place
    and setting me on the road to something bigger.
Thanks to Sally and Tim for revealing the juice in the spiritual path
    and the juice in endless exploration.
Thanks to Jerry and Carol Anne for telling me about Yunus Emre.
Thanks to the Echo Chamber and the Lauras for being an early audience
    and offering moral support.
Thanks to Sadie for proofreading this book.
And thanks to you for reading it!
