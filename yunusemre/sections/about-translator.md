What business do I have translating these poems? I'm not a scholar, a Turk,
a Sufi, or even a Muslim. The only qualifications I have are a love and
respect for Yunus Emre's work, a little skill at writing in verse, and the
power of obsession. I started these translations just to deepen my
appreciation for the poems, with no thought of publication,
but it somehow turned into a burning passion. I've felt Yunus's spirit
working its way deeper into my bones and my relationship with the
eternal intensifying. So I've already been richly rewarded, but if these
translations can help even one other person open their heart or
draw closer to God, I'll be even more pleased. I know that I
can't do Yunus's poems justice, but I hope that at least
I haven't done them an injustice.

I'm certain to have made mistakes.
But they don't need to be set in stone, and I'd like to make this a
living work as long as I have the time and energy to maintain it.
If you're in a position to catch mistakes or suggest improvements,
please email me at:

**jesse.crossen@gmail.com**

Let me know what you're thinking, tell me how to credit you,
and I'll try to include it in a future edition. Maybe one day these will be
"folk translations." Or feel free to email me just to tell me how the poems
strike you; I always love to meet people and hear about their lives.
If you're interested in my original poetry, essays, stories,
and travel writing, you can find them at:

**https://hinterlander.substack.com**

I hope you enjoy reading this book as much as I've enjoyed creating it!
