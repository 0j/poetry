Yunus Emre was born in 1238 in Anatolia, a region of what is now Turkey,
and died around 1320.
He followed a mystical and freethinking branch of Islam called Sufism,
but he had little interest in sectarian differences.
He was one of the first to compose poetry in the everyday Turkish
of his people rather than in Persian or Arabic,
and his poems were really folk poetry, in the same sense as folksongs
and folktales. Long before they were printed in books, they were passed
along by word of mouth, recited at gatherings of religious orders and
laypeople and sung as songs and hymns. Over 700 years later, he remains one of
the most beloved poets in the Turkish language, seen by many as a folk hero
and a saint. Not much is known about the details of his life,
but his work reveals a fascinating character:
a tortured soul, a biting wit, a self-deprecating humor,
a tough love for humanity, an ecstatic love for God,
and a deep current of divine inspiration.
Just like us, Yunus lived in a time of upheaval and
rapid change, and his poems have a vital message,
showing us that reaching out for the divine
can be both an escape from the world's struggles and an
all-consuming struggle of its own. His intense and
uncompromising humanism speaks to people in all times,
all places, and all walks of life.
