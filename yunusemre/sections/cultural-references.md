When searching for imagery and culture from the western literary tradition to
draw on, it's hard to find a single direct parallel to Yunus Emre.
But looking within a few generations of his life, I can put together
pieces of it from the culture of feudal Europe.
In what is now Italy, Saint Francis of Assisi had dedicated his life to
mysticism, renunciation of the world, and connection to God and nature.
In what is now France, Guillaume de Machaut was composing haunting music
full of longing for a seemingly impossible love.
And in England, Geoffrey Chaucer was writing lucid verse with all the
colorful vitality of the middle ages.
Put these together and I think that's as close as I can get to describing
the spirit of Yunus Emre.
To learn about the life and legend of Francis of Assisi, I've drawn from
Nikos Kazantzakis's beautiful *Saint Francis*.
To catch the tone of longing, Machaut's music speaks to me with no need for words.
For the poetic voice, Chaucer's English is unfortunately too distant to be much help.
But since many of the early audience for Yunus's poetry would have been farmers,
I've taken inspiration from Wendell Berry, America's own farmer-poet,
particularly his earthy spirituality and plainspoken, unadorned style.
Having grown up in a rural part of the American Southeast,
I've drawn inspiration from that regional dialect, for example in expressions
like "run your mouth" and "some folks." Hopefully these will come across
to readers from other parts of the world.
