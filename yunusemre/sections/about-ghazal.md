The *ghazal* is an ancient poetic form that originated in Arabic and eventually
spread to many other languages with an Arabic or Persian influence. Yunus wrote
many poems in this form, some holding strictly to the form and some playing with
it a little. The best way to illustrate the conventions is probably by example,
so here's a short *ghazal* I've written as a demonstration:

|    This world to cast my eyes about,
|    too big to be so wise about.
|
|    And me a tiny wooden boat
|    that stormy seas will rise about.
|
|    For seeking you I can't be still;
|    a frightened bird still flies about.
|
|    This ocean has more parting ways
|    than we can say goodbyes about.
|
|    The sailor, salt in all his pores,
|    just shakes his head and sighs about.
|
|    Our ancient wounds which have no name,
|    a child feels and cries about.
|
|    But sea without, and sea within,
|    no surface to capsize about.
|
|    What's Hinterlander hinting at
|    that even silence lies about?

Notice the following points:

1. The poem is organized into couplets. Each couplet is supposed to stand on its own as a
   statement, or a statement followed by the "proof" of that statement.
2. The lines can be in any meter, with any number of syllables, as long as
   they are all the same. Here the meter is iambic tetrameter.
3. Many of the lines end in "about," which in this case is the refrain.
   The refrain can be just a word, as in this example, or a longer phrase.
4. Just before each refrain is the rhyme, which in this case sounds like
   "-ize." Both lines of the first couplet must end with this rhyme and
   the refrain, and so must the second line of every other couplet.
5. The final couplet has a special role, because it includes the poet's pen
   name, often gets more personal, and often emphasizes the poem's theme.
   Sometimes there will be a play on words with the pen name, for example in this
   case there's a play on "Hinterlander" and "hint." In the poems in this
   book, you might want to know that "*yunus*" also means "dolphin" in
   Turkish, so when the final couplet references diving into the sea, it's a play
   on words that's been lost in translation. If you think about it, this is a
   very clever way for poets to sign their work, because it's hard to
   remove or change the name without spoiling the poem's conclusion.

Another part of the *ghazal* tradition, which I haven't reproduced in the
example above, is that poems usually have a theme of unrequited love, somewhat
like the "courtly love" tradition in medieval Europe. However, in religious
*ghazals* this love is meant as a metaphor for a seeker's intense longing
for union with God. Common tropes are the garden, the *bulbul* (which I've
translated as "nightingale"), and the rose. The garden can be seen as
representing the world, with God as the gardener, the nightingale as
the alter-ego of the poet, and the rose as representing the beloved,
or the beauty of earthly forms.

One aspect of the *ghazal* that's nearly impossible to translate is the rhyme,
because even for the shortest allowable poem with five couplets, you'd
need to find a collection of six words that rhymed, and these are very
unlikely to fit into the six rhymes of the original without straining
the meaning. I've only managed to do this with trivial rhymes like "-ing"
as in the poem I've titled *Spring*. Sometimes even the refrain presents a
challenge because the word order is different in Turkish and English,
and it won't always naturally go at the end of an English sentence.
Sometimes it only gets through in a ghostly form as a repeated concept,
for example in *The Hour of Death*, where all the refrain lines have something
to do with talking.

The *semai* is a Turkish form of folk poem that's something like a
*ghazal* with a specific meter and an internal rhyme scheme.
You can imagine the couplets broken into quatrains,
with each line having eight syllables, and the rhyme scheme
is ABAB CCCB DDDB and so on. Often there is no rhyme in the "B" lines,
and instead all of them are the same line repeated as a refrain.
As in the *ghazal*, the final quatrain includes the poet's pen name.
These would typically be sung to a tune.
In a few cases I've used this kind of rhyme scheme even when it wasn't
present in the original because I like the sound of it and because in
a way it compensates for all the times when I failed to rhyme.
For some poems, I've dropped the form entirely and translated into
blank verse or free verse, which allows for a more conversational tone.
While I was working on *Let Down Thy Veil*, the original reminded me
so much of Shakespeare that I just had to turn it into an Elizabethan
sonnet, even though a few couplets had to be dropped to get it down to
fourteen lines. But one way or another, I think at least a little bit
of the *ghazal* or *semai* spirit remains in all of the poems.

