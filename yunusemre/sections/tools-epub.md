**Translation:**

* TurEng (excellent general-purpose dictionary)
* Wiktionary (good for etymology and grammar)
* Reverso Context (good for usage and idioms)
* The glossary from Yunus Emre's *Divan* compiled by ~Dr. Mustafa Tatcı~
* *www.nedirnedemek.com* (more obscure words)
* *nedir.ileilgili.org* (regional and archaic words)
* Google Translate (sparingly for gestalt meaning)
* Wikipedia (for background research)

**Versification:** RhymeZone, *www.powerthesaurus.org*

**Design:** Inkscape, ImageMagick
