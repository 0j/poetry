When trying to decide how to illustrate this book, I immediately thought of
Turkish culture's rich tradition of illuminated manuscripts and calligraphy.
I took some inspiration from the style known as *tezhip*, but to keep the
cost of the book down I decided not to use color. Each illustration is formed
from a single line, never crossing itself, which you could see as a symbol
of the unity of God and creation, the thread of life, the labyrinth,
or the winding spiritual journey that Yunus describes so well. Scribes of the
medieval world used to copy books by hand with ornate letters to start the
chapters and fanciful pictures in the margins. We don't normally do that
anymore, but I hope that making the book itself a little prettier can add
something to the message and to your enjoyment of it.
