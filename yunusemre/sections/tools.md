**Translation:**

* TurEng (excellent general-purpose dictionary)
* Wiktionary (good for etymology and grammar)
* Reverso Context (good for usage and idioms)
* The glossary from Yunus Emre's *Divan* compiled by ~Dr. Mustafa Tatcı~
* *www.nedirnedemek.com* (more obscure words)
* *nedir.ileilgili.org* (regional and archaic words)
* Google Translate (sparingly for gestalt meaning)
* Wikipedia (for background research)

**Versification:** RhymeZone, *www.powerthesaurus.org*

**Typesetting and Design:** LuaLaTeX, ImageMagick

**Illustrations:** Skedio, Inkscape

**Typefaces:** All text is set in Whitman by Kent Lew,
    except for Turkish text which is set in Noto Serif.
