#!/usr/bin/env python3

import os
import sys
import glob
import re
import math
from collections import namedtuple

MATCH_IMAGE = re.compile(r'\s*<image\s+(.*)xlink')
MATCH_ATTR = re.compile(r'(\w+)="([^"]+)"')
MATCH_MATRIX = re.compile(r'matrix\((.*)\)')
MATCH_PATH = re.compile(r'stroke="#000000"[^>]*\bd="([^"]+)"')

Position = namedtuple('Position', ['width', 'height', 'tx', 'ty', 'sx', 'sy'])
Point = namedtuple('Point', ['x', 'y'])
PointRef = namedtuple('PointRef', ['pathidx', 'point'])
Join = namedtuple('Join', ['d', 'a', 'b'])

# parse the position of the template image
def parsepos(image):
  attrs = { k: v for (k, v) in MATCH_ATTR.findall(image) }
  x = float(attrs['x'])
  y = float(attrs['y'])
  width = float(attrs['width'])
  height = float(attrs['height'])
  (sx, _, _, sy, tx, ty) = [ float(v) for v in
    MATCH_MATRIX.match(attrs['transform']).group(1).split(' ') ]
  return Position(
    width, height, - (x + tx), - (y + ty), 1.0 / sx, 1.0 / sy)

# parse the svg file
def parse(inpath):
  with open(inpath, 'r') as f:
    pos = None
    paths = []
    for line in f:
      m = MATCH_IMAGE.match(line)
      if m:
        newpos = parsepos(m.group(1))
        if ((pos is None) or
            (newpos.width * newpos.height > pos.width * pos.height)):
          pos = newpos
      m = MATCH_PATH.search(line)
      if m:
        paths.append(parsepath(m.group(1).strip()))
  # transform all path points to be relative to the template image
  return (pos.width, pos.height,
          [transformpath(path, pos) for path in paths])

# transform a path using the given position spec
def transformpath(path, pos):
  return tuple([ transformpoint(point, pos) for point in path ])
def transformpoint(point, pos):
  return Point((point.x + pos.tx) * pos.sx,
               (point.y + pos.ty) * pos.sy)

# extract endpoints of the paths
def parsepath(code):
  points = []
  coords = [ float(c) for c in re.split(r'[^-\d.]+', code) if c ]
  for i in range(0, len(coords), 2):
    points.append(Point(coords[i], coords[i+1]))
  return tuple(points)

# join paths whose ends are within a threshold
def joinall(paths, threshold):
  points = []
  # collect all endpoints
  for (i, path) in enumerate(paths):
    points.append(PointRef(i, path[0]))
    points.append(PointRef(i, path[-1]))
  joins = []
  for (i, a) in enumerate(points):
    for (j, b) in enumerate(points):
      # distances are the same in either direction
      if j <= i: continue
      # don't allow a path to join to itself
      if a.pathidx == b.pathidx: continue
      joins.append(Join(
        math.pow((a.point.x - b.point.x), 2) +
        math.pow((a.point.y - b.point.y), 2),
        a, b))
  # put the closest distances first
  joins.sort(key=lambda d: d.d)
  # we can't make more joins than we have paths
  joins = joins[:len(paths)]
  # apply the threshold
  threshold2 = math.pow(threshold, 2)
  joins = [ join for join in joins if join.d <= threshold2 ]
  # join the paths
  for join in joins:
    # get the two paths
    a = paths[join.a.pathidx]
    b = paths[join.b.pathidx]
    ai = a
    bi = b
    # reverse the paths as needed so the points match up
    astart = join.a.point == a[0]
    bstart = join.b.point == b[0]
    if (astart):          # x--o x--o / x--o o--x
      a = tuple(reversed(a))
    if (not bstart):      # o--x o--x
      b = tuple(reversed(b))
    # join paths
    joined = a[:-1] + b
    # replace all references to the original path with the joined version
    for (i, path) in enumerate(paths):
      if path == ai or path == bi:
        paths[i] = joined
  # remove duplicates
  return list(set(paths))

def pathcode(path):
  # make the path go from left to right
  if (path[0].x > path[-1].x):
    path = tuple(reversed(path))
  # encode the points
  s = 'M%f %fC' % (path[0].x, path[0].y)
  for point in path[1:]:
    s += '%f,%f ' % (point)
  return s

def svg(width, height, paths):
  s = (('<svg version="1.1" width="%f" height="%f"' % (width, height))
       + ' xmlns="http://www.w3.org/2000/svg">')
  style = 'stroke="black" fill="none"'
  for path in paths:
    s += '  <path %s d="%s"/>' % (style, pathcode(path))
  s += '</svg>'
  return s

def process(inpath, outpath):
  (width, height, paths) = parse(inpath)
  paths = joinall(paths, 2.0)
  if (len(paths) != 1):
    print('  WARNING: %d paths in output' % len(paths))
  with open(outpath, 'w') as f:
    f.write(svg(width, height, paths))

def main():
  # collect paths
  inpath = sys.argv[1]
  outpath = sys.argv[2]
  if inpath.endswith('.svg') and outpath.endswith('.svg'):
    jobs = [ (inpath, outpath) ]
  else:
    jobs = [ (path, os.path.join(outpath, os.path.basename(path)))
             for path in glob.glob(os.path.join(inpath, '*.svg')) ]
  # process paths
  for (inpath, outpath) in jobs:
    print('JOINING: %s => %s' % (inpath, outpath))
    process(inpath, outpath)
main()
