#!/usr/bin/env python3

import os
import io
import re
import sys
import json
import zipfile
import datetime
import argparse
import markdown
from markdown.extensions.smarty import SmartyExtension
from markdown.preprocessors import Preprocessor
from markdown.blockprocessors import BlockProcessor
from markdown.inlinepatterns import SimpleTagInlineProcessor, Pattern
from markdown.extensions import Extension
import xml.etree.ElementTree as etree

def main(args):
  with open(args.inpath, 'r') as f:
    manifest = json.load(f)

  with zipfile.ZipFile(args.outpath, 'w') as epub:
    epub.writestr('mimetype', b'application/epub+zip', zipfile.ZIP_STORED)
    write_container(manifest, epub)
    write_opf(manifest, epub)
    write_style(manifest, epub)
    write_toc(manifest, epub)
    write_content(manifest, epub, os.path.dirname(args.inpath))

def write_container(manifest, epub):
  with epub.open('META-INF/container.xml', 'w') as raw:
    with io.TextIOWrapper(raw, encoding='utf-8') as f:
      f.write('\n'.join([
        '<?xml version="1.0"?>',
        '<container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">',
        '  <rootfiles>',
        '    <rootfile full-path="OEBPS/content.opf" media-type="application/oebps-package+xml"/>',
        '  </rootfiles>',
        '</container>']))

def write_opf(manifest, epub):
  # compose metadata
  meta = []
  meta.append('  <dc:title>%s</dc:title>' % manifest['title'])
  meta.append('  <dc:creator opf:role="aut">%s</dc:creator>' % manifest['creator'])
  if ('translator' in manifest):
    meta.append('  <dc:creator opf:role="trl">%s</dc:creator>' % manifest['translator'])
  meta.append('  <dc:subject>Poetry</dc:subject>')
  if ('description' in manifest):
    meta.append('  <dc:description>%s</dc:description>' % manifest['description'])
  meta.append('  <dc:language>en-US</dc:language>')
  meta.append(
    '  <dc:rights>'+
        'Creative Commons Attribution-ShareAlike 4.0 International License'+
      '</dc:rights>')
  meta.append('  <dc:publisher>hinterlander.substack.com</dc:publisher>')
  meta.append('  <dc:coverage>Worldwide</dc:coverage>')
  meta.append('  <dc:identifier id="BookID">%s</dc:identifier>' % manifest['isbn'])
  meta.append('  <meta name="cover" content="Images/Cover.jpg"/>')
  meta.append('\n'.join([
    '  <dc:relation>',
    '    Title="%s"' % manifest['title'],
    '    Relation="%s"' % manifest['isbn'],
    '    format="print"',
    '  </dc:relation>']))
  # compose chunks
  items = []
  itemrefs = []
  guide = []
  for chunk in manifest['chunks']:
    items.append(
      '  <item id="%s" href="%s.xhtml" media-type="application/xhtml+xml"/>' %
        (chunk['slug'], chunk['slug']))
    itemrefs.append(
      '  <itemref idref="%s" />' % chunk['slug'])
    if (chunk['context'] == 'toc'):
      guide.append('  <reference type="toc" title="%s" href="%s.xhtml"/>' %
        (chunk['title'], chunk['slug']))
    elif ('start' in chunk):
      guide.append('  <reference type="start" title="%s" href="%s.xhtml"/>' %
        (chunk['title'], chunk['slug']))
  # compose file
  with epub.open('OEBPS/content.opf', 'w') as raw:
    with io.TextIOWrapper(raw, encoding='utf-8') as f:
      f.write('\n'.join([
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="BookID" version="2.0" >',
        '<metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">'
          ]+meta+[
        '</metadata>',
        '<manifest>',
        '  <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml" />',
        '  <item id="style" href="stylesheet.css" media-type="text/css" />'
          ]+items+[
        '</manifest>',
        '<spine toc="ncx">'
          ]+itemrefs+[
        '</spine>',
        '<guide>'
          ]+guide+[
        '</guide>',
        '</package>']))

def write_toc(manifest, epub):
  with epub.open('OEBPS/toc.ncx', 'w') as raw:
    with io.TextIOWrapper(raw, encoding='utf-8') as f:
      f.write('\n'.join([
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">',
        '<head>',
        '  <meta name="dtb:uid" content="%s"/>' % manifest['isbn'],
        '  <meta name="dtb:depth" content="1"/>',
        '  <meta name="dtb:totalPageCount" content="0"/>',
        '  <meta name="dtb:maxPageNumber" content="0"/>',
        '</head>',
        '<docTitle>',
        '  <text>%s</text>' % manifest['title'],
        '</docTitle>',
        '<navMap>',
        '']))
      i = 0
      for chunk in manifest['chunks']:
        i = i + 1
        f.write('\n'.join([
          '  <navPoint id="%s" playOrder="%d">' % (chunk['slug'], i),
          '    <navLabel>',
          '      <text>%s</text>' % chunk['title'],
          '    </navLabel>',
          '    <content src="%s.xhtml"/>' % chunk['slug'],
          '  </navPoint>',
          '']))
      f.write('\n'.join([
        '</navMap>',
        '</ncx>']))

def write_style(manifest, epub):
  with epub.open('OEBPS/stylesheet.css', 'w') as raw:
    with io.TextIOWrapper(raw, encoding='utf-8') as f:
      f.write('\n'.join([
        # web-style paragraphs
        'p {',
        '  text-indent: 0;',
        '  margin-top: 1em;',
        '}',
        # align headings with the text
        'h1 {',
        '  text-align: left;',
        '}',
        # center the cover image
        'h1.cover {',
        '  text-align: center;',
        '}'
        # inline poem
        'div.poem {',
        '  margin-left: 2em;',
        '}',
        # poem lines
        'p.line0 {',
          'margin-top: 0;',
          'margin-bottom: 0;',
          'text-indent: 0;',
        '}',
        'p.line1 {',
          'margin-top: 0;',
          'margin-bottom: 0;',
          'text-indent: 0;',
          'margin-left: 4em;',
        '}',
        'p.line2 {',
          'margin-top: 0;',
          'margin-bottom: 0;',
          'text-indent: 0;',
          'margin-left: 8em;',
        '}'
      ]))

def write_content(manifest, epub, dirname):
  for chunk in manifest['chunks']:
    if (chunk['context'] == 'cover'):
      with epub.open('OEBPS/Images/Cover.jpg', 'w') as output:
        with open(os.path.join(dirname, chunk['path']), 'rb') as input:
          output.write(input.read())
    with epub.open('OEBPS/%s.xhtml' % chunk['slug'], 'w') as raw:
      with io.TextIOWrapper(raw, encoding='utf-8') as f:
        if (chunk['context'] == 'toc'):
          content = format_toc(chunk['title'], manifest)
        else:
          content = format_content(chunk['title'],
                           os.path.join(dirname, chunk['path']),
                           chunk['context'])
        f.write(wrap_content(chunk['title'], content))

def wrap_content(title, content):
  return '\n'.join([
    '<?xml version="1.0" encoding="UTF-8" standalone="no"?>',
    '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
    '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en" lang="en">',
    '  <head>',
    '    <title>%s</title>' % title,
    '    <link rel="stylesheet" type="text/css" href="stylesheet.css" />',
    '  </head>',
    '  <body>',
         content,
    '  </body>',
    '</html>'])

def format_toc(title, manifest):
  lines = []
  lines.append('    <h1>%s</h1>' % title)
  lines.append('    <ul>')
  for chunk in manifest['chunks']:
    if (chunk['context'] in ('cover', 'toc')): continue
    title = chunk['title']
    if ('toctitle' in chunk):
      title = chunk['toctitle']
    if (chunk['context'] != 'poem'):
      title = '<i>%s</i>' % title
    lines.append('      <li><a href="%s.xhtml">%s</a></li>' %
                  (chunk['slug'], title))
  lines.append('    </ul>')
  return '\n'.join(lines)

def format_content(title, path, context):
  if (context == 'cover'):
    return '\n'.join([
      '<h1 class="cover" title="Cover">',
      '  <img alt="Cover" height="100%%" src="Images/Cover.jpg"/> <br/>',
      '  &nbsp;',
      '</h1>'
    ])
  ext = []
  ext.append(SmartyExtension(substitutions={
      'left-single-quote': '‘',
      'right-single-quote': '’',
      'left-double-quote': '“',
      'right-double-quote': '”'
    }))
  ext.append(IntlExtension())
  ext.append(TypographyExtension())
  if (context == 'poem'):
    ext.append(StripHeadersExtension())
    ext.append(VerseExtension(force=True))
  else:
    ext.append(VerseExtension())
    ext.append(AutolinkExtension())
    ext.append(AutomailExtension())
  s = '<h1 class="%s">%s</h1>' % (context, title)
  with open(path, 'r', encoding='utf-8') as f:
    s += markdown.markdown(f.read(),
      extensions=ext, output_format='xhtml')
  return s

class TypographyExtension(Extension):
  def extendMarkdown(self, md):
    md.preprocessors.register(Typography(),
      'typography', 175)
class Typography(Preprocessor):
  def run(self, lines):
    out = []
    for line in lines:
      line = line.replace('${today}',
        datetime.date.today().strftime('%B %d, %Y'))
      line = line.replace('``', '“') # TeX double left quote
      line = line.replace("''", '”') # TeX double right quote
      line = line.replace('---', '—') # em dash
      line = line.replace('--', '–') # en dash
      out.append(line)
    return out

class StripHeadersExtension(Extension):
  def extendMarkdown(self, md):
    md.preprocessors.register(StripHeaders(),
      'stripheaders', 175)
class StripHeaders(Preprocessor):
  def run(self, lines):
    out = []
    for line in lines:
      if line.startswith('#'): continue
      out.append(line)
    return out

class IntlExtension(Extension):
  def extendMarkdown(self, md):
    md.inlinePatterns.register(
      SimpleTagInlineProcessor(r'(\~)([^~]+)(\~)', 'i'),
        'intl', 175)

class AutolinkExtension(Extension):
  def extendMarkdown(self, md):
    md.inlinePatterns.register(
      Autolink(r'(\bhttps?://[^\s*]+)', self), 'autolink', 175)
class Autolink(Pattern):
  def handleMatch(self, m):
    el = etree.Element('a')
    el.set('href', m.group(2))
    el.text = m.group(2).split('//', 1)[1]
    return el

class AutomailExtension(Extension):
  def extendMarkdown(self, md):
    md.inlinePatterns.register(
      Automail(r'(\b[\w.]+@[\w.]+\b)', self), 'automail', 1)
class Automail(Pattern):
  def handleMatch(self, m):
    el = etree.Element('a')
    el.set('href', 'mailto:%s' % m.group(2))
    el.text = m.group(2).replace('@', '&#x40;')
    return el

class VerseExtension(Extension):
  def __init__(self, force=False):
    self.force = force
  def extendMarkdown(self, md):
    md.parser.blockprocessors.register(
      Verse(md.parser, force=self.force),
      'verse', 175)
class Verse(BlockProcessor):
  RE_HEADER = r'^\| {0,4}'

  def __init__(self, parser, force=False):
    BlockProcessor.__init__(self, parser)
    self.force = force

  def in_verse(self):
    return (self.parser.state.isstate('verse') or
            (self.force and not self.parser.state.isstate('verseline')))

  def test(self, parent, block):
    if self.in_verse():
      return True
    return re.match(self.RE_HEADER, block)

  def run(self, parent, blocks):
    if self.in_verse():
      return self.verse(parent, blocks)
    original_block = blocks[0]
    end = len(blocks)
    for i, block in enumerate(blocks):
      if not re.match(self.RE_HEADER, block):
        end = i
        break
    verselines = '\n'.join(blocks[0:end]).split('\n')
    verselines = [
        re.sub(self.RE_HEADER, '', line) for line in verselines]
    div = etree.SubElement(parent, 'div')
    div.set('class', 'poem')
    self.parser.state.set('verse')
    self.parser.parseChunk(div, '\n'.join(verselines))
    self.parser.state.reset()
    for i in range(0, end):
      blocks.pop(0)
    return True

  def verse(self, parent, blocks, indent='    '):
    if not self.parser.state.isstate('verse'):
      parent = etree.SubElement(parent, 'div')
      parent.set('class', 'poem')
    self.parser.state.set('verseline')
    for block in blocks:
      lines = [line for line in block.split('\n') if line.strip() != '']
      for i, line in enumerate(lines):
        level = 0
        while line.startswith(indent):
          line = line[len(indent):]
          level += 1
        self.parser.parseBlocks(parent, [line])
        p = self.lastChild(parent)
        if p is not None:
          p.set('class', 'line%d' % level)
          if i == 0:
            p.set('style', 'margin-top: 0.75em;')
    blocks[:] = []
    self.parser.state.reset()
    return True

parser = argparse.ArgumentParser(description='Package content into an epub.')
parser.add_argument('inpath', type=str,
                    help='the path to a json manifest to package')
parser.add_argument('outpath', type=str,
                    help='the path to write the epub file to')
args = parser.parse_args()
main(args)
