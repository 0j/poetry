#!/bin/bash

convert -density 36 $1[$2] -background white -flatten \
    /mnt/chromeos/MyFiles/Downloads/pdf-$2.png
