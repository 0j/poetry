default: ye

# remove all generated files
clean:
	-rm pdf/*

# rebuild when the makefile or any library file changes
LIBFILES:=$(shell find lib) makefile

# common params when generating PDF output
PDFCOMMON=--output-directory=../pdf \
		  --interaction=nonstopmode \
		  --halt-on-error

# allow easy loading of local packages
TEXENV=TEXINPUTS=::../lib LUAINPUTS=::../lib OPENTYPEFONTS=::../fonts

# Yunus Emre translations #####################################################

YEMD:=$(shell find yunusemre/poems yunusemre/sections)

ye: pdf/yunusemre.pdf pdf/yunusemre-dual-language.pdf pdf/yunusemre-cover.pdf epub/yunusemre.epub

YEBASE=cd yunusemre && $(TEXENV) lualatex $(PDFCOMMON)

# easily printable dual-language version for translation work
pdf/yunusemre-dual-language.pdf: yunusemre/dual-language.tex $(YEMD) $(LIBFILES)
	$(YEBASE) --jobname=yunusemre-dual-language dual-language.tex

# actual book
YEBUILD=$(YEBASE) --jobname=yunusemre book.tex && cd -
pdf/yunusemre.pdf: yunusemre/book.tex $(YEMD) $(LIBFILES) yegraphics
	$(YEBUILD) && $(YEBUILD) # two-pass to position full-bleed graphics

# cover
pdf/yunusemre-cover.pdf: yunusemre/graphics/yunusemre-cover.svg
	inkscape --export-area-page --export-dpi=300 \
		yunusemre/graphics/yunusemre-cover.svg \
		-o pdf/yunusemre-cover.pdf

# illustrations
YESKETCHES=$(shell find yunusemre/graphics/skedio -name '*.svg')
yegraphics: yunusemre/graphics/.touch
yunusemre/graphics/.touch: $(YESKETCHES) makefile scripts/joinskedio.py
	./scripts/joinskedio.py yunusemre/graphics/skedio yunusemre/graphics && \
		touch yunusemre/graphics/.touch

# ebook
yunusemre/graphics/epub-cover.jpg: yunusemre/graphics/yunusemre-cover.svg
	inkscape --export-area-page --export-dpi=311 \
		yunusemre/graphics/yunusemre-cover.svg \
		-o /tmp/epub-cover.png
	convert /tmp/epub-cover.png \
		-gravity East -crop 1600x2560+0+0 +repage \
		yunusemre/graphics/epub-cover.jpg

epub/yunusemre.epub: yunusemre/epub.json scripts/epub.py $(YEMD) yunusemre/graphics/epub-cover.jpg
	./scripts/epub.py yunusemre/epub.json epub/yunusemre.epub
